Analyse de sondages
===================

Code utilisé dans la série de _live_ intitulée
« La Geste des statistiques et de l’opinion ». Ce code présente une méthode
simple pour mettre en évidence les tendances mesurées par les sondages. Il
lit une série de sondages dans un fichier pour en produire la courbe des
tendances à l’aide de la méthode des moindres carrés.

La documentation du code est
[disponible en ligne](https://ylebars.frama.io/analysesondage/ "Documentation du code").

**Table des matières**

* [Lancer le code](#lancer-le-code "Lancer le code")
* [Format de données](#format-de-données "Format de données")
* [Générer la documentation](#générer-la-documentation "Générer la documentation")
* [Contrat de diffusion](#contrat-de-diffusion "Contrat de diffusion")

Lancer le code
--------------

Ce code fonctionne sur tous les systèmes supportant
[Python 3](https://www.python.org/ "Python scripting language") Pour le faire
tourner, il vous faut un interprète Python 3. Le code utilise les modules
suivants :

* [SciPy](https://www.scipy.org/ "SciPy") ;
* [NumPy](http://www.numpy.org/ "NumPy") ;
* [Matplotlib](https://matplotlib.org/ "Matplotlib") ;
* [Seaborn](https://seaborn.pydata.org/ "Seaborn") ;
* [Pandas](https://pandas.pydata.org/ "Pandas") ;
* [Scikit-learn](https://scikit-learn.org/stable/index.html "Scikit-learn") ;
* [Plotly](https://plotly.com/python/ "Plotly") ;
* [Dash](https://dash.plotly.com/ "Dash").

Pour le lancer, il suffit d’exécuter le script nommé « analyseSondages.py »
situé dans le répertoire « src ». La syntaxe est la suivante :

```bash
analyseSondages.py [-h] [-v] [-e nomExclus] confiance nom_fichier
```

Avec `confiance` la taille de l’intervalle de confiance en pourcentage (la
norme dans les sondages d’opinions est de regarder l’intervalle de confiance
à 95 %) et `nom_fichier` le chemin d’accès au fichier contenant les sondages
à analyser (il y en de disponibles dans le répertoire `ressources`).

L’option `-h` (ou `--help`) produit l’affichage d’un message d’aide et met
fin au programme, tandis que l’option `-v` (ou `--version`) produit
l’affichage de la version du programme et met fin à l’exécution. Enfin,
l’option `-e` (ou `--exclure`) permet de transmettre un fichier au format CSV
dans lequel chaque valeur est le nom d’un candidat à exclure de l’extrapolation
par faute d’un nombre suffisant de données.

Format de données
-----------------

Le fichier contenant les sondages doit être un fichier au format CSV, encodé
en UTF-8. Le format CSV (_comma separated values_) consiste en un fichier
texte, dans lequel sur chaque ligne les données sont séparées par une
virgule. En passant à la ligne, on crée une nouvelle ligne de données.

La première ligne indique les dates de début des sondages. La première valeur
de la ligne n’est pas significative, nous conseillons de lui donner la valeur
« Date de début du sondage ». La deuxième valeur n’est pas significative non
plus, nous conseillons de la laisser vide. Ensuite, chaque valeur doit
donner une date de début de sondage, encodée au format ISO « YYYY-MM-DD », à
savoir les quatre chiffres de l’année, un tiret, les deux chiffres du mois,
un tiret et les deux chiffres du jour.

La deuxième ligne indique les dates de début des sondages. La première valeur
de la ligne n’est pas significative, nous conseillons de lui donner la valeur
« Date de fin du sondage ». La deuxième valeur n’est pas significative non
plus, nous conseillons de la laisser vide. Ensuite, chaque valeur doit
donner une date de fin de sondage, encodée au format ISO « YYYY-MM-DD », à
savoir les quatre chiffres de l’année, un tiret, les deux chiffres du mois,
un tiret et les deux chiffres du jour.

Sur la troisième ligne, la première valeur n’est pas significative, mais nous
avons l’habitude de l’utiliser comme information sur les données contenue sur
cette ligne, à savoir la taille des échantillons en nombre de personnes. La
deuxième valeur n’est pas significative. Ensuite, les valeurs doivent donner
la taille des échantillons sondés en nombre de personnes pour chaque sondage.

Les lignes suivantes sont les lignes des intentions de vote associées à
chaque candidat. Elles ont toutes le même format : la première valeur est le
nom du candidat, la deuxième le code de la couleur associée au candidat sur
les graphiques. Enfin, pour chaque sondage, il doit être indiqué les
intentions de vote en %.

Il n’y a pas de limite sur le nombre de sondages analysés, ni sur le nombre
de candidats.

Une liste des couleurs possibles avec un échantillon est visible à cette URL :

<https://matplotlib.org/stable/gallery/color/named_colors.html>

Le fichier des exclus doit être également un fichier au format CSV encodé en
UTF-8. Seule la première ligne de ce fichier est prise en compte. Elle doit
comporter les noms des exacts des candidats à exclure de l’interpolation,
avec la même graphie que les noms indiqués dans le fichier de sondage.

Générer la documentation
------------------------

La documentation du code est gérée à l’aide de
[Sphinx](https://www.sphinx-doc.org/ "Sphinx") ainsi que
[Graphviz](https://graphviz.org/ "Graphviz") et utilise le thème
[Read the Docs](https://readthedocs.org/ "Read the Docs")
(`sphinx-rtd-theme`). Pour la générer, allez dans le répertoire `doc`. En
exécutant la commande `make`, vous obtiendrez la liste des formats de
documentation possible. Par exemple, pour générer la documentation au format
HTML, réalisez la commande suivante :

```bash
make html
```

Un sous répertoire sera alors créé dans `doc/build`. Ainsi, dans l’exemple
précédent, la documentation générée sera placée dans le répertoire
`doc/build/html`, la première page étant intitulée `index.html`.

Contrat de diffusion
--------------------

Ce programme est sous contrat de distribution CeCILL v2.1. Vous pouvez le
modifier et le redistribuer, à la condition que vous en citiez l’auteur
original et que vous ne changez pas le contrat de distribution.

Le texte complet du contrat est disponible
[en ligne](https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html "Contrat CeCILL v2.1").

Copyright © 2021–2023 [Yoann LE BARS](http://le-bars.net/yoann/ "Vu d’ici").
