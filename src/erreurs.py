#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Classes pour la gestion des erreurs.

:module: erreurs.py
:author: Yoann LE BARS

Ce programme est sous contrat de distribution CeCILL v2.1. Vous pouvez le
modifier et le redistribuer, à la condition que vous en citiez l’auteur
original et que vous ne changez pas le contrat de distribution.

Le texte complet du contrat est disponible dans le fichier « LICENSE » et à
l’adresse suivante :

https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html
"""


import os


class Erreur (Exception):
    """
    Classe de base pour la gestion des erreurs.
    """

    pass


class ConfianceHorsLimites (Erreur):
    """
    Exception lancée lorsque l’intervalle de confiance demandé est en dehors
    de [0 ; 100].
    """

    pass


class FichierInexistant (Erreur):
    """
    Exception lancée lorsque le chemin d’accès donné ne mène vers aucun
    fichier.

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
    """

    nom_fichier: os.PathLike

    def __init__(self, nom_fichier: os.PathLike) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué
            l’erreur.
        """

        self.nom_fichier = nom_fichier


class NomInexistant (Erreur):
    """
    Exception lancée lorsqu’un nom de candidat est introuvable.

    :param str nom: Nom ayant provoqué l’erreur.
    """

    nom: str

    def __int__(self, nom: str) -> None:
        """
        Initialisation de la classe.

        :param str nom: Nom ayant provoqué l’erreur.
        """

        self.nom = nom


class ErreurJour (Erreur):
    """
    Classe de base pour la gestion des erreurs provoquées par les jours.
    """

    pass


class JourInvalide (ErreurJour):
    """
    Exception lancée lorsqu’un nombre de jours n’est pas sous un format
    valide.

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
    :param int ligne: Numéro de la ligne ayant produit l’erreur.
    :param int colonne: Numéro de colonne de la valeur ayant produit l’erreur.
    """

    nom_fichier: os.PathLike
    ligne: int
    colonne: int

    def __init__(self, nom_fichier: os.PathLike, ligne: int, colonne: int) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
        :param int ligne: Numéro de la ligne ayant produit l’erreur.
        :param int colonne: Numéro de colonne de la valeur ayant produit
            l’erreur.
        """

        self.nom_fichier = nom_fichier
        self.ligne = ligne
        self.colonne = colonne


class NbJoursInco (ErreurJour):
    """
    Exception lancée lorsque le nombre de jours de fin de sondages est
    différent du nombre de jours de début de sondages.

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
    """

    nom_fichier: os.PathLike

    def __init__(self, nom_fichier: os.PathLike) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué
            l’erreur.
        """

        self.nom_fichier = nom_fichier


class FinAvantDebut (ErreurJour):
    """
    Exception lancée lorsque la date de fin du sondage précède sa date de
    début.

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
    :param int colonne: Numéro de colonne de la valeur ayant produit
            l’erreur.
    """

    nom_fichier: os.PathLike
    colonne: int

    def __init__(self, nom_fichier: os.PathLike, colonne: int) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué
            l’erreur.
        :param int colonne: Numéro de colonne de la valeur ayant produit
            l’erreur.
        """

        self.nom_fichier = nom_fichier
        self.colonne = colonne


class ErreurEchantillon (Erreur):
    """
    Classe de base pour la gestion des erreurs provoquées par les
    échantillons.
    """

    pass


class NbEchantillonsInco (ErreurEchantillon):
    """
    Exception lancée lorsque le nombre d’échantillons est différent du nombre
    de dates de sondage.

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
    """

    nom_fichier: os.PathLike

    def __init__(self, nom_fichier: os.PathLike) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué
            l’erreur.
        """

        self.nom_fichier = nom_fichier


class EchantillonInvalide (ErreurEchantillon):
    """
    Exception lancée lorsqu’une taille d’échantillon n’est pas sous un
    format valide.

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
    :param int ligne: Numéro de la ligne ayant produit l’erreur.
    :param int colonne: Numéro de colonne de la valeur ayant produit
        l’erreur.
    """

    nom_fichier: os.PathLike
    ligne: int
    colonne: int

    def __init__(self, nom_fichier: os.PathLike, ligne: int, colonne: int) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué
            l’erreur.
        :param int ligne: Numéro de la ligne ayant produit l’erreur.
        :param int colonne: Numéro de colonne de la valeur ayant produit
            l’erreur.
        """

        self.nom_fichier = nom_fichier
        self.ligne = ligne
        self.colonne = colonne


class EchantillonInval (ErreurEchantillon):
    """
    Exception lancée lorsqu’une taille d’échantillon n’est pas sous un
    format valide.

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
    :param str nom_candidat: Nom du candidat ayant produit l’erreur.
    :param int colonne: Numéro de colonne de la valeur ayant produit
        l’erreur.

    :todo: Cette classe fait doublon avec la précédente, elle sert de
        transition entre la version 1 et la version 2 du programme. À terme,
        il faudra n’en conserver qu’une.
    """

    nom_fichier: os.PathLike
    nom_candidat: str
    colonne: int

    def __init__(self, nom_fichier: os.PathLike, nom_candidat: str,
                 colonne: int) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué
            l’erreur.
        :param str nom_candidat: Numéro de la ligne ayant produit l’erreur.
        :param int colonne: Numéro de colonne de la valeur ayant produit
            l’erreur.
        """

        self.nom_fichier = nom_fichier
        self.nom_candidat = nom_candidat
        self.colonne = colonne


class EchantillonHorsLimites (ErreurEchantillon):
    """
    Exception lancée lorsqu’une taille d’échantillon est inférieure à 1.

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
    :param int ligne: Numéro de la ligne ayant produit l’erreur.
    :param int colonne: Numéro de colonne de la valeur ayant produit
        l’erreur.
    """

    nom_fichier: os.PathLike
    ligne: int
    colonne: int

    def __init__(self, nom_fichier: os.PathLike, ligne: int, colonne: int) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué
            l’erreur.
        :param int ligne: Numéro de la ligne ayant produit l’erreur.
        :param int colonne: Numéro de colonne de la valeur ayant produit
            l’erreur.
        """

        self.nom_fichier = nom_fichier
        self.ligne = ligne
        self.colonne = colonne


class CouleurInvalide (Erreur):
    """
    Exception lancée lorsqu’une couleur demandée ne fait pas partie de la
    liste des couleurs disponibles.

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
    :param int ligne: Numéro de la ligne ayant produit l’erreur.
    :param int colonne: Numéro de colonne de la valeur ayant produit
        l’erreur.
    """

    nom_fichier: os.PathLike
    ligne: int
    colonne: int

    def __init__(self, nom_fichier: os.PathLike, ligne: int, colonne: int) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué
            l’erreur.
        :param int ligne: Numéro de la ligne ayant produit l’erreur.
        :param int colonne: Numéro de colonne de la valeur ayant produit
            l’erreur.
        """

        self.nom_fichier = nom_fichier
        self.ligne = ligne
        self.colonne = colonne


class CouleurInval (Erreur):
    """
    Exception lancée lorsqu’une couleur demandée ne fait pas partie de la
    liste des couleurs disponibles.

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoque l’erreur.
    :param str nom_candidat: Nom du candidat ayant produit l’erreur.
    :param int colonne: Numéro de colonne de la valeur ayant produit
        l’erreur.

    :todo: Cette classe fait doublon avec la précédente, elle sert de
        transition entre la version 1 et la version 2 du programme. À terme,
        il faudra n’en conserver qu’une.
    """

    nom_fichier: os.PathLike
    nom_candidat: str
    colonne: int

    def __init__(self, nom_fichier: os.PathLike, nom_candidat: str, colonne: int) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué
            l’erreur.
        :param str nom_candidat: Nom du candidat ayant produit l’erreur.
        :param int colonne: Numéro de colonne de la valeur ayant produit
            l’erreur.
        """

        self.nom_fichier = nom_fichier
        self.nom_candidat = nom_candidat
        self.colonne = colonne


class ErreurIntention (Erreur):
    """
    Classe de base pour la gestion des erreurs provoquées par les intentions
    de vote.
    """

    pass


class IntentionInvalide (ErreurIntention):
    """
    Exception lancée lorsque l’intention de vote n’est pas sous un format
    valide.

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
    :param int ligne: Numéro de la ligne ayant produit l’erreur.
    :param int colonne: Numéro de colonne de la valeur ayant produit l’erreur.
    """

    nom_fichier: os.PathLike
    ligne: int
    colonne: int

    def __init__(self, nom_fichier: os.PathLike, ligne: int, colonne: int) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué
            l’erreur.
        :param int ligne: Numéro de la ligne ayant produit l’erreur.
        :param int colonne: Numéro de colonne de la valeur ayant produit
            l’erreur.
        """

        self.nom_fichier = nom_fichier
        self.ligne = ligne
        self.colonne = colonne


class IntentionInval (ErreurIntention):
    """
    Exception lancée lorsque l’intention de vote n’est pas sous un format
    valide.

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
    :param str nom_candidat: Nom du candidat ayant produit l’erreur.
    :param int colonne: Numéro de colonne de la valeur ayant produit l’erreur.

    :todo: Cette classe fait doublon avec la précédente, elle sert de
        transition entre la version 1 et la version 2 du programme. À terme,
        il faudra n’en conserver qu’une.
    """

    nom_fichier: os.PathLike
    nom_candidat: str
    colonne: int

    def __init__(self, nom_fichier: os.PathLike, nom_candidat: str,
                 colonne: int) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué
            l’erreur.
        :param str nom_candidat: Nom du candidat ayant produit l’erreur.
        :param int colonne: Numéro de colonne de la valeur ayant produit
            l’erreur.
        """

        self.nom_fichier = nom_fichier
        self.nom_candidat = nom_candidat
        self.colonne = colonne


class IntentionHorsLimites (ErreurIntention):
    """
    Exception lancée lorsque l’intention de vote n’est pas dans l’intervalle
    [0 ; 100].

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
    :param str nom_candidat: Nom du candidat dont les intentions de vote ont
        provoqué l’erreur.
    :param int colonne: Numéro de colonne de la valeur ayant produit l’erreur.
    """

    nom_fichier: os.PathLike
    nom_candidat: str
    colonne: int

    def __init__(self, nom_fichier: os.PathLike, nom_candidat: str,
                 colonne: int) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué
            l’erreur.
        :param str nom_candidat: Nom du candidat dont les intentions de vote
            ont provoqué l’erreur.
        :param int colonne: Numéro de colonne de la valeur ayant produit
            l’erreur.
        """

        self.nom_fichier = nom_fichier
        self.nom_candidat = nom_candidat
        self.colonne = colonne


class IntentionHorsLim (ErreurIntention):
    """
    Exception lancée lorsque l’intention de vote n’est pas dans l’intervalle
    [0 ; 100].

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
    :param int ligne: Numéro de ligne de la valeur ayant produit l’erreur.
    :param int colonne: Numéro de colonne de la valeur ayant produit l’erreur.
    """

    nom_fichier: os.PathLike
    ligne: int
    colonne: int

    def __init__(self, nom_fichier: os.PathLike, ligne: int,
                 colonne: int) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué
            l’erreur.
        :param int ligne: Numéro de ligne de la valeur ayant produit
            l’erreur.
        :param int colonne: Numéro de colonne de la valeur ayant produit
            l’erreur.
        """

        self.nom_fichier = nom_fichier
        self.ligne = ligne
        self.colonne = colonne


class NbIntentionsInco (ErreurIntention):
    """
    Exception lancée lorsque la série des intentions de vote envers un
    candidat est différente du nombre de dates de sondage.

    :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué l’erreur.
    :param str nom_candidat: Nom du candidat dont la série des intentions de
        vote a provoqué l’erreur.
    :param int taille_serie: Nombre d’entrées dans la série.
    :param int nombre_sondages: Nombre de sondages dans la série.
    """

    nom_fichier: os.PathLike
    nom_candidat: str
    taille_serie: int
    nombre_sondage: int

    def __init__(self, nom_fichier: os.PathLike, nom_candidat: str,
                 taille_serie: int, nombre_sondages: int) -> None:
        """
        Initialisation de la classe.

        :param os.PathLike nom_fichier: Chemin d’accès ayant provoqué
            l’erreur.
        :param str nom_candidat: Nom du candidat dont la série des intentions
            de vote a provoqué l’erreur.
        :param int taille_serie: Nombre de sondages renseignés dans la série.
        :param int nombre_sondages: Nombre total de sondages.
        """

        self.nom_fichier = nom_fichier
        self.nom_candidat = nom_candidat
        self.taille_serie = taille_serie
        self.nombre_sondages = nombre_sondages
