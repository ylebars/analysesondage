#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Programme simple calculant la matrice covariances-variances d’une série de
sondages.

:module: calculMatCov
:author: Yoann LE BARS

Pour le lancer, il suffit d’exécuter le script nommé « calculMatCov.py »
situé dans le répertoire « src ». La syntaxe est la suivante :

::

    calculMatCov [-h] [-v] nomFichierSondages

Avec `nomFichierSondages` le chemin d’accès au fichier contenant les sondages
à analyser (il y en a de disponibles dans le répertoire `ressources`).

L’option `-h` (ou `--help`) produit l’affichage d’un message d’aide et met
fin au programme, tandis que l’option `-v` (ou `--version`) produit
l’affichage de la version du programme et met fin à l’exécution.

Ce programme est sous contrat de distribution CeCILL v2.1. Vous pouvez le
modifier et le redistribuer, à la condition que vous en citiez l’auteur
original et que vous ne changez pas le contrat de distribution.

Le texte complet du contrat est disponible dans le fichier « LICENSE » et à
l’adresse suivante :

https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html
"""


from erreurs import *
from os import path
import csv
import calculCov
import seaborn as sn
import matplotlib.pyplot as plt
import pandas as pd
from constantes import __version__


def calcul_matrice(nom_fichier_sondages: os.PathLike) -> None:
    """
    Calcul la matrice covariances-variances d’une série de sondages, la
    stocke dans un fichier CSV et la représente graphiquement.

    :param os.PathLike nom_fichier_sondages: Chemin d’accès vers le fichier
        contenant les sondages.

    :raises FichierInexistant: Si jamais le chemin d’accès vers le fichier
        des sondages est invalide.
    """

    if (not path.exists(nom_fichier_sondages)) or (not path.isfile(nom_fichier_sondages)):
        raise FichierInexistant(nom_fichier_sondages)

    # Liste des candidats à l’élection.
    liste_candidats = []

    with open(nom_fichier_sondages, newline='', encoding='utf-8') as fichier:
        # Lecteur du fichier CSV.
        lecteur = csv.reader(fichier)

        liste_candidats = [line[0] for line in csv.reader(fichier.readlines())][3:]

    # Matrice covariances-variances.
    mat_covvar = [[calculCov.traitement(nom_fichier_sondages, [candidat1, candidat2])
                   for candidat2 in liste_candidats] for candidat1 in liste_candidats]
    # Table des données.
    table = pd.DataFrame(mat_covvar, index=liste_candidats, columns=liste_candidats)

    plt.title('Matrice covariances-variances')
    sn.heatmap(table, annot=True, fmt='f')
    plt.show()


# Partie du code à exécuter au début du script.
if __name__ == '__main__':
    import argparse
    import sys

    # Analyseur de la ligne de commande.
    analyseur = argparse.ArgumentParser(
        description='Programme déterminant la matrice covariances-variances'
                    'dans une série de sondages.')
    analyseur.add_argument('-v', '--version', action='version',
                           version='%(prog)s ' + __version__,
                           help='Affiche la version du programme et quitte.')
    analyseur.add_argument('nomFichierSondages',
                           help='Nom du fichier contenant les sondages.')

    # Arguments de la ligne de commande.
    args = analyseur.parse_args()

    try:
        calcul_matrice(args.nomFichierSondages)
    except FichierInexistant as ex:
        print(f'Erreur 1 : pas de fichier nommé « {ex.nom_fichier} ».',
              file=sys.stderr)
        sys.exit(1)
    except IntentionInvalide as ex:
        print(f'Erreur 2 : dans le fichier « {ex.nom_fichier} », ligne '
              f'{ex.ligne}, la valeur {ex.colonne} est invalide : elle doit '
              f'être un nombre à virgule flottante.', file=sys.stderr)
        sys.exit(2)
    except IntentionHorsLim as ex:
        print(f'Erreur 3 : dans le fichier « {ex.nom_fichier} », ligne '
              f'{ex.ligne}, la valeur {ex.colonne} doit être supérieure ou '
              f'égale à 0 et inférieure ou égale à 100.', file=sys.stderr)
        sys.exit(3)
    except NomInexistant as ex:
        print(f'Erreur 4 : pas de candidat nommé « {ex.nom} ».',
              file=sys.stderr)
        sys.exit(4)
