#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Ensemble de constantes utiles pour l’analyse de sondages.

:module: constantes.py
:author: Yoann LE BARS

Ce programme est sous contrat de distribution CeCILL v2.1. Vous pouvez le
modifier et le redistribuer, à la condition que vous en citiez l’auteur
original et que vous ne changez pas le contrat de distribution.

Le texte complet du contrat est disponible dans le fichier « LICENSE » et à
l’adresse suivante :

https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html
"""


# Numéro de version majeur.
__majeur__ = '2'
# Numéro de version mineur.
__mineur__ = '1'
# Version du programme.
__version__ = __majeur__ + '.' + __mineur__

# Nombre de secondes dans une journée.
nbSecondesDansJour = 86400.

# Réponses positives.
positives = ['o', 'O', 'oui', 'Oui', 'OUI']
# Réponses négatives.
negatives = ['n', 'N', 'non', 'Non', 'NON']
# Liste des réponses possibles.
listeReponses = positives + negatives
