#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Programme simple déterminant les corrélations entre deux candidats dans une
série de sondages.

:module: calculCov
:author: Yoann LE BARS

Pour le lancer, il suffit d’exécuter le script nommé « calculCorr.py »
situé dans le répertoire « src ». La syntaxe est la suivante :

::

    calculCov.py [-h] [-v] nom_fichier_sondages nom_fichier_selectionnes

Avec `nomFichierSondages` le chemin d’accès au fichier contenant les sondages
à analyser (il y en a de disponibles dans le répertoire `ressources`) et
`nom_fichier_selectionnes` le fichier indiquant les deux candidats à évaluer.

L’option `-h` (ou `--help`) produit l’affichage d’un message d’aide et met
fin au programme, tandis que l’option `-v` (ou `--version`) produit
l’affichage de la version du programme et met fin à l’exécution.

Au sujet du coefficient de la variance :

https://fr.wikipedia.org/wiki/Variance_(math%C3%A9matiques)

Au sujet de la covariance :

https://fr.wikipedia.org/wiki/Covariance

Ce programme est sous contrat de distribution CeCILL v2.1. Vous pouvez le
modifier et le redistribuer, à la condition que vous en citiez l’auteur
original et que vous ne changez pas le contrat de distribution.

Le texte complet du contrat est disponible dans le fichier « LICENSE » et à
l’adresse suivante :

https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html
"""


from erreurs import *
from os import path
import csv
import numpy as np
from constantes import __version__


def traitement(nom_fichier_sondages: os.PathLike, liste_candidats_courants: list) -> float:
    """
    Calcul la covariance entre deux candidats.

    :param os.PathLike nom_fichier_sondages: Chemin d’accès vers le fichier
        contenant les sondages.
    :param list liste_candidats_courants: Liste des candidats à analyser.

    :returns: La covariance.
    :rtype: float

    :raises FichierInexistant: Si jamais le chemin d’accès au fichier des
        sondages est incorrect.
    :raises IntentionInvalide: Si jamais une intention de vote n’est pas
        exprimée sous forme de nombre à virgule flottante.
    :raises IntentionHorsLim: Si jamais une intention de vote est inférieure
        à 0 % ou supérieure à 100 %.
    :raises NomInexistant: Si jamais un des noms de candidats donnés est introuvable.
    """

    if (not path.exists(nom_fichier_sondages)) or (not path.isfile(nom_fichier_sondages)):
        raise FichierInexistant(nom_fichier_sondages)

    # Nom du premier candidat à tester
    nom1 = liste_candidats_courants[0]
    # Nom du deuxième candidat à tester
    nom2 = liste_candidats_courants[1]

    # Liste des sondages pour chaque candidat sélectionné.
    sondages = []
    # Liste des numéros des lignes de sondages testées.
    numeros_ligne = []
    # Indique si oui ou non le nom du premier candidat a été trouvé.
    trouve_nom1 = False
    # Indique si oui ou non le nom du deuxième candidat a été trouvé.
    trouve_nom2 = False
    with open(nom_fichier_sondages, newline='', encoding='utf8') as fichier_courant:
        # Lecteur du fichier CSV.
        lecteur_csv = csv.reader(fichier_courant)
        # Numéro de la ligne courante.
        n_ligne = 0
        for ligne in lecteur_csv:
            n_ligne += 1
            if ligne[0] in liste_candidats_courants:
                sondages.append(ligne[2:])
                numeros_ligne.append(n_ligne)
                if ligne[0] in nom1:
                    trouve_nom1 = True
                if ligne[0] in nom2:
                    trouve_nom2 = True
    if not trouve_nom1:
        raise NomInexistant(nom1)
    if not trouve_nom2:
        raise NomInexistant(nom2)
    if nom1 == nom2:
        sondages.append(sondages[0])
        numeros_ligne.append(numeros_ligne[0])

    # Première série à tester.
    candidat1 = []
    # Deuxième série à tester.
    candidat2 = []
    print(f'{nom1} / {nom2}')
    for i in range(len(sondages[0])):
        # Valeur courante pour le candidat numéro 1.
        x = sondages[0][i]
        # Valeur courante pour le candidat numéro 2.
        y = sondages[1][i]
        if (x != '') and (y != ''):
            try:
                # Intention de vote courante pour le candidat x.
                valeur_x = float(x)
            except ValueError:
                raise IntentionInvalide(nom_fichier_sondages, numeros_ligne[0], i + 3)
            if valeur_x < 0. or valeur_x > 100.:
                raise IntentionHorsLim(nom_fichier_sondages, numeros_ligne[0], i + 3)
            candidat1.append(valeur_x)
            try:
                # Intention de vote courante pour le candidat y.
                valeur_y = float(y)
            except ValueError:
                raise IntentionInvalide(nom_fichier_sondages, numeros_ligne[1], i + 3)
            if valeur_y < 0. or valeur_y > 100.:
                raise IntentionHorsLim(nom_fichier_sondages, numeros_ligne[1], i + 3)
            candidat2.append(valeur_y)

    return np.cov(candidat1, candidat2)[0][1]


# Partie du code à exécuter au début du script.
if __name__ == '__main__':
    import argparse
    import sys

    # Analyseur de la ligne de commande.
    analyseur = argparse.ArgumentParser(
        description='Programme déterminant les corrélations entre deux '
                    'candidats dans une série de sondages.')
    analyseur.add_argument('-v', '--version', action='version',
                           version='%(prog)s ' + __version__,
                           help='Affiche la version du programme et quitte.')
    analyseur.add_argument('nom_fichier_sondages',
                           help='Nom du fichier contenant les sondages.')
    analyseur.add_argument('nom_fichier_selectionnes',
                           help='Nom du fichier contenant les noms des '
                                'candidats à analyser.')
    # Arguments de la ligne de commande.
    args = analyseur.parse_args()

    # Nom du fichier contenant la liste des candidats à analyser.
    nom_fichier_selectionnes = args.nom_fichier_selectionnes
    if (not path.exists(nom_fichier_selectionnes)) or (not path.isfile(nom_fichier_selectionnes)):
        print(f'Erreur 1 : pas de fichier nommé '
              f'« {nom_fichier_selectionnes} ».', file=sys.stderr)
        sys.exit(1)
    with open(nom_fichier_selectionnes, newline='', encoding='utf-8') as fichier:
        # Lecteur du fichier CSV.
        lecteur = csv.reader(fichier)
        # Liste des noms des candidats à tester.
        liste_candidats = next(lecteur)

    try:
        print(traitement(args.nom_fichier_sondages, liste_candidats))
    except FichierInexistant as ex:
        print(f'Erreur 2 : pas de fichier nommé « {ex.nom_fichier} ».',
              file=sys.stderr)
        sys.exit(2)
    except IntentionInvalide as ex:
        print(f'Erreur 3 : dans le fichier « {ex.nom_fichier} », ligne '
              f'{ex.ligne}, la valeur {ex.colonne} est invalide ; elle doit '
              f'être un nombre à virgule flottante.', file=sys.stderr)
        sys.exit(3)
    except IntentionHorsLim as ex:
        print(f'Erreur 4 : dans le fichier « {ex.nom_fichier} », ligne '
              f'{ex.ligne}, la valeur {ex.colonne} doit être supérieure ou '
              f'égale à 0 et inférieure ou égale à 100.', file=sys.stderr)
        sys.exit(4)
    except NomInexistant as ex:
        print(f'Erreur 5 : pas de candidat nommé « {ex.nom} ».',
              file=sys.stderr)
        sys.exit(5)
