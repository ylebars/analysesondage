#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Ensemble de fonctions utilitaires pour l’analyse de sondages.

:module: utilitaires
:author: Yoann LE BARS

Ce programme est sous contrat de distribution CeCILL v2.1. Vous pouvez le
modifier et le redistribuer, à la condition que vous en citiez l’auteur
original et que vous ne changez pas le contrat de distribution.

Le texte complet du contrat est disponible dans le fichier « LICENSE » et à
l’adresse suivante :

https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html
"""


from typing import List, Tuple
from erreurs import *
from collections import namedtuple
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import calculCorr
from sklearn.svm import SVR
from constantes import nbSecondesDansJour


# Triplet pour le calcul de l’erreur d’approximation.
TripletErreur = namedtuple("TripletErreur", "jour a b")

# Pointeurs vers la figure et ses axes.
fig, ax = plt.subplots()
# Dictionnaire des courbes.
dicCourbes = {}
# Dictionnaire des plages d’erreurs.
dicErreurs = {}
# Liste des candidats
candidats = []
# Liste des nuages de points des intentions de votes.
listePoints = []


def traite_date(chaine_valeur: str, nom_fichier: os.PathLike, ligne: int,
                colonne: int) -> np.datetime64:
    """
    Convertit une chaîne de caractères en une date et renvoie une erreur en
    cas de format invalide.

    :param str chaine_valeur: La chaîne de caractères à convertir.
    :param os.PathLike nom_fichier: Chemin d’accès au fichier contenant les
        intentions de votes.
    :param int ligne: Indice de la ligne courante.
    :param int colonne: Indice de la colonne courante.

    :returns: La date sous forme de `datetime`.
    :rtype: np.datetime64

    :raises JourInvalide: Si jamais une date n’est pas renseignée sous
        forme d’une chaîne de caractères valide.
    """

    try:
        return np.datetime64(chaine_valeur)
    except ValueError:
        raise JourInvalide(nom_fichier, ligne, colonne)


def calcul_duree(debut: datetime, fin: datetime, colonne: int,
                 nom_fichier: os.PathLike) -> timedelta:
    """
    Fonction calculant la durée d’un sondage.

    :param datetime debut: Jour de démarrage du sondage.
    :param datetime fin: Jour de fin du sondage.
    :param int colonne: Colonne du fichier de données en cours de traitement.
    :param os.PathLike nom_fichier: Nom du fichier en cours de traitement.

    :returns: La durée du sondage.
    :rtype: timedelta

    :raises FinAvantDebut: Si jamais la date de fin est antérieur à la date
        de début.
    """

    if fin < debut:
        raise FinAvantDebut(nom_fichier, colonne)

    return fin - debut


def converti_echantillon(t: str, nom_fichier: os.PathLike,
                         liste_echantillons: list[str]) -> int:
    """
    Fonction convertissant une chaîne de caractères en taille
    d’échantillon sous forme d’un entier et gérant les erreurs.

    :param str t: La chaine de caractère à convertir.
    :param str nom_fichier: Nom du fichier en cours de traitement.
    :param list[str] liste_echantillons: Liste de tous les échantillons.

    :return: La taille de l’échantillon.
    :rtype: int

    :raises EchantillonInvalide: Si jamais la valeur convertie ne correspond
        pas à un entier.
    :raises EchantillonHorsLimites: Si jamais la taille de l’échantillon est
        strictement inférieure à 1.
    """

    try:
        # Taille de l’échantillon.
        taille = int(t)
    except ValueError:
        raise EchantillonInvalide(nom_fichier, 3,
                                  liste_echantillons.index(t) + 3)
    if taille < 1:
        raise EchantillonHorsLimites(nom_fichier, 3,
                                     liste_echantillons.index(t) + 3)

    return taille


def traite_intentions(q: float, nom_fichier: os.PathLike, nom_candidat: str, ligne: List[str],
                      t_ech: np.array) -> Tuple[np.array, np.array, np.array]:
    """
    Traite les intentions de vote envers un candidat.

    :param float q: Coefficient pour le calcul de l’intervalle de confiance.
    :param os.PathLike nom_fichier: Chemin d’accès au fichier en cours de traitement.
    :param str nom_candidat: Nom du candidat en cours de traitement.
    :param list[str] ligne: Contenu de la ligne en cours de traitement.
    :param list t_ech: Liste des tailles d’échantillon.

    :returns: Les arrays complétées des intentions de
        votes, des inverses des écarts-types des sondages et des intervalles
        de confiances.
    :rtype: Tuple[np.array, np.array, np.array]

    :raises IntentionInvalide: Si une intention de vote ne correspond pas à
        un nombre à virgule flottante.
    :raises IntentionHorsLimites: Si une intention de vote n’est pas
        comprise entre 0 et 100.
    """

    print(f'Traitement du candidat {nom_candidat}')
    # Conversion de la ligne courante en tableau Numpy (floats), incluant
    # les NAN. On part du principe que '' = NAN.
    # Ligne contenant les intentions de votes sous forme de flottants.
    line_array = []
    for ix in range(2, len(ligne)):
        # Valeur en cours de traitement.
        valeur = ligne[ix]
        if valeur == '':
            line_array.append(np.nan)
        else:
            try:
                line_array.append(float(ligne[ix]))
            except ValueError:
                raise IntentionInval(nom_fichier, nom_candidat, ix + 1)
    line_array = np.array(line_array)

    # Vérification des valeurs minimum et maximum des pourcentages.
    # Lève une exception si le pourcentage est inférieur à 0 ou supérieur
    # à 100.
    if any(np.logical_or(line_array < 0., line_array > 100.)):
        for ix in range(len(line_array)):
            if line_array[ix] > 100. or line_array[ix] < 0.:
                raise IntentionHorsLimites(nom_fichier, nom_candidat, ix + 3)
    
    intentions = line_array

    # Normalisation des pourcentages dans [0, 1].
    normalisees = intentions / 100.
    # Sélection des indices où les valeurs sont égales à 0 ou 1.
    normalisees_limit_idx = np.logical_or(normalisees == 0., normalisees == 1.) 
    s = np.empty_like(normalisees)

    # Si valeur == 1 ou valeur == 0 -> s = 1.e-7
    s[normalisees_limit_idx] = 1.e-7

    # Sinon, s = ((normalisee * (1. - normalisee) / taille échantillon)^(1/2)
    # Vu que tous les tableaux ont la même taille, on doit tous les filtrer
    # pour retirer les valeurs correspondant aux valeurs normalisées à 1 ou 0
    # La syntaxe s[~normalisees_limit_idx] permet de faire ça : en prenant le
    # tableau inverse des valeurs à 0 ou 1, on a le tableau des valeurs à
    # traiter.
    s[~normalisees_limit_idx] = np.power(
        (normalisees[~normalisees_limit_idx] * (1. - normalisees[~normalisees_limit_idx]))
        /
        t_ech[~normalisees_limit_idx],
        0.5)

    # On inverse s pour obtenir w.
    w = np.power(s, -1)

    # Calcul des intervalles de confiance.
    confiances = q * s * 100.

    return intentions, w, confiances


def extrapole(w: np.ndarray, durees: list[timedelta],
              jours_dispo: np.ndarray, intentions: np.ndarray):
    """
    Extrapole les intentions de votes pour un candidat donné et affiche la
    courbe associée.

    :param np.ndarray w: Liste des inverses des écarts types pour chaque
        sondage.
    :param list[timedelta] durees: Liste des durées des sondages.
    :param np.ndarray jours_dispo: Liste des jours disponibles pour le candidat
        courant.
    :param np.ndarray intentions: Listes des intentions de vote pour le candidat
        courant.

    :returns: Série extrapolée des intentions de vote.
    """

    # Poids de chaque sondage.
    poids = [w[i]
             + (nbSecondesDansJour / (durees[i].astype('timedelta64[s]').astype(np.int32))) for i in range(len(durees))]
    # Système d’extrapolation.
    svr = SVR(kernel='rbf', gamma='scale', C=1.0, epsilon=0.1)
    return svr.fit(np.array(jours_dispo).reshape(-1, 1),
                   intentions[~np.isnan(intentions)], np.array(poids)[~np.isnan(intentions)])


def selectionne_ligne(evenement):
    """
    Gestion de l’évènement lorsque que l’utilisateur sélectionne une ligne de
    la légende.

    :param evenement: Descripteur de l’évènement.
    """

    # Ligne de la légende.
    ligne_legend = evenement.artist
    # La courbe sélectionnée.
    ligne_courbe = dicCourbes[ligne_legend]
    # Erreurs de la courbe sélectionnée.
    ligne_erreur = dicErreurs[ligne_legend]
    # Détermine s’il faut montrer ou cacher la courbe.
    visible = not ligne_courbe.get_visible()
    ligne_courbe.set_visible(visible)
    ligne_erreur.set_visible(visible)
    ligne_legend.set_alpha(1.0 if visible else 0.5)
    fig.canvas.draw()


def selectionne_point(label: str):
    """
    Gestion de l’évènement lorsque l’utilisateur sélectionne un point de la
    liste des candidats.

    :param str label: Label de la case cochée.
    """

    # Position du label.
    pos = candidats.index(label)
    # on sélectionne les enfants et on boucle sur l'objet sélectionné.
    for objet in listePoints[pos].get_children():
        objet.set_visible(not objet.get_visible())
    fig.canvas.draw()


def format_corr(nom_fichier_sondages: os.PathLike, candidat1: str,
                candidat2: str) -> str:
    """
    Formate les chaînes de caractères pour la sortie des corrélations et
    valeurs p.

    :param os.PathLike nom_fichier_sondages: Chemin d’accès au fichier
        contenant les sondages.
    :param str candidat1: Nom du premier candidat à tester.
    :param str candidat2: Nom du deuxième candidat à tester.

    :returns: Une chaîne de caractères proprement formatée contenant
        corrélation et valeur p.
    :rtype: str
    """

    # Corrélation et valeur p.
    r, p = calculCorr.traitement(nom_fichier_sondages, [candidat1, candidat2])
    return '({:.4f}, {:.4f})'.format(r, p)
