#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Programme simple évaluant les tendances dans un sondage d’opinion.

:module: analyseSondages
:author: Yoann LE BARS

Pour le lancer, il suffit d’exécuter le script nommé « analyseSondages.py »
situé dans le répertoire « src ». La syntaxe est la suivante :

::

    analyseSondages.py [-h] [-v] [-e nomExclus] confiance nom_fichier

Avec `confiance` la taille de l’intervalle de confiance en pourcentage (la
norme dans les sondages d’opinions est de regarder l’intervalle de confiance à
95 %) et `nom_fichier` le chemin d’accès au fichier contenant les sondages à
analyser (il y en a de disponibles dans le répertoire `ressources`).

L’option `-h` (ou `--help`) produit l’affichage d’un message d’aide puis met
fin au programme, tandis que l’option `-v` (ou `--version`) produit
l’affichage de la version du programme puis met fin à l’exécution et l’option
`-e` (ou `--exclure`) permet de transmettre un fichier au format CSV dans
lequel chaque valeur est le nom d’un candidat à exclure de l’extrapolation
par faute d’un nombre suffisant de données.

En cliquant sur une ligne de la légende, on fait apparaître ou disparaître
l’extrapolation pour le candidat associé.

Une liste des couleurs possibles avec un échantillon est visible à cette URL :

https://matplotlib.org/stable/gallery/color/named_colors.html

Des informations concernant les intervalles de confiance :

https://fr.wikipedia.org/wiki/Intervalle_de_confiance

Au sujet des machines à vecteurs de support (*support-vector machines*, SVM)
et leur application aux régressions (*SVM for regression*, SVR) :

https://en.wikipedia.org/wiki/Support-vector_machine

Ce programme est sous contrat de distribution CeCILL v2.1. Vous pouvez le
modifier et le redistribuer, à la condition que vous en citiez l’auteur
original et que vous ne changez pas le contrat de distribution.

Le texte complet du contrat est disponible dans le fichier « LICENSE » et à
l’adresse suivante :

https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html
"""


from erreurs import *
from os import path
import csv
import math
from scipy import special
import numpy as np
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
from matplotlib.widgets import CheckButtons
from scipy.interpolate import interp1d
import statistics
from utilitaires import dicCourbes, dicErreurs, candidats, fig, ax,\
    listePoints, traite_date, calcul_duree, traite_intentions, extrapole,\
    selectionne_ligne, selectionne_point
from constantes import __version__


def traitement(confiance_demandee: float, nom_fichier: os.PathLike,
               nom_exclus: str = '') -> None:
    """
    Fonction traitant le fichier de sondage.

    :param float confiance_demandee: Intervalle de confiance demandé.
    :param str nom_fichier: Chemin d’accès au fichier de sondage.
    :param str nom_exclus: Chemin d’accès vers le fichier indiquant les
        candidats à exclure de l’extrapolation.

    :raises ConfianceHorsLimites: Si jamais l’intervalle de confiance demandé
        n’est pas compris entre 0 et 100.
    :raises FichierInexistant: Si jamais le chemin d’accès à l’un des
        fichiers est incorrect.
    :raises NbJoursInco: Si jamais il n’y a pas le même nombre de jours de
        début de sondage et de fin de sondage.
    :raises CouleurInvalide: Si jamais une couleur demandée n’est pas dans la
        liste des couleurs disponibles.
    """

    if (confiance_demandee <= 0.) or (confiance_demandee >= 100.):
        raise ConfianceHorsLimites()

    if (not path.exists(nom_fichier)) or (not path.isfile(nom_fichier)):
        raise FichierInexistant(nom_fichier)

    # Liste des courbes.
    courbes = []
    # Liste des erreurs.
    liste_erreurs = []

    with open(nom_fichier, newline='', encoding='utf-8') as fichier:
        # Lecteur du fichier CSV.
        lecteur = csv.reader(fichier)

        print('Traitement des dates de début.')
        # Ligne courante du fichier.
        ligne = next(lecteur)
        # Liste des jours de début des sondages
        # timedelta64(8, 'h') parce qu’on fixe l’heure de début à 8 h.
        jours_debut = np.array(
            [traite_date(ligne[i], nom_fichier, 1, i + 1) + np.timedelta64(8, 'h') for i in range(2, len(ligne))])
        # Date de départ des entrées.
        debut = jours_debut[0]
        # Nombre d’entrées de sondage.
        nb_entrees = len(jours_debut)

        print('Traitement des dates de fin.')
        ligne = next(lecteur)
        # Liste des jours de fin des sondages.
        # timedelta64(20, 'h') parce qu’on fixe l’heure de fin à 20 h.
        jours_fin = np.array(
            [traite_date(ligne[i], nom_fichier, 2, i + 1) + np.timedelta64(20, 'h') for i in range(2, len(ligne))])
        if len(jours_fin) != nb_entrees:
            raise NbJoursInco(nom_fichier)
        # Durées des sondages.
        durees = [calcul_duree(jours_debut[i], jours_fin[i], i + 3, nom_fichier) for i in range(nb_entrees)]
        # Date des sondages.
        dates = [jours_debut[i] + (durees[i] / 2.) for i in range(nb_entrees)]
        # Liste des jours de sondages par rapport à la date du premier.
        jours = (dates - debut).astype(float)

        ligne = next(lecteur)
        # Tailles des échantillons sondés.
        # Supposés être des entiers positifs -> EchantillonInvalide et EchantillonHorsLimites 
        # sont levées si ce n’est pas bon.
        t_ech = []
        for string_digit in ligne[2:]:
            try:
                digit = int(string_digit)
            except ValueError:
                raise EchantillonInvalide(nom_fichier, 3, ligne[2:].index(string_digit) + 3)
            if digit < 1:
                raise EchantillonHorsLimites(nom_fichier, 3, ligne.index(string_digit) + 1)
            t_ech.append(digit)

        # Il doit y avoir autant d’échantillons que de jours de sondage.
        if len(t_ech) != nb_entrees:
            raise NbEchantillonsInco(nom_fichier)
        t_ech = np.asarray(t_ech)

        # Liste des candidats à exclure de l’extrapolation.
        exclus = []
        if nom_exclus:
            if (not path.exists(nom_exclus)) or (not path.isfile(nom_exclus)):
                raise FichierInexistant(nom_fichier)
            with open(nom_exclus, newline='', encoding='utf-8') as fichierExclus:
                # Lecteur de CSV.
                lecteur_exclus = csv.reader(fichierExclus)
                exclus = next(lecteur_exclus)

        # liste des couleurs possible.
        couleurs_possibles = [*mcolors.BASE_COLORS] + [*mcolors.TABLEAU_COLORS] + [*mcolors.CSS4_COLORS]
        # Coefficient de l’intervalle de confiance.
        q = math.sqrt(2.) * special.erfinv(confiance_demandee / 100.)

        # Index de la ligne en cours d’analyse.
        n_ligne = 2
        for ligne in lecteur:
            n_ligne += 1
            # Nom du candidat courant.
            nom = ligne[0]
            candidats.append(nom)
            # Couleur du candidat.
            couleur = ligne[1]
            if couleur not in couleurs_possibles:
                raise CouleurInvalide(nom_fichier, n_ligne, 2)
            # Liste des jours à supprimer, intentions de votes, inverses des
            # écarts types et intervalles de confiance en points de
            # pourcentage.
            intentions, w, confiances = traite_intentions(q, nom_fichier, nom, ligne, t_ech)
            # Liste des jours disponibles pour ce candidat.
            # À nouveau, la syntaxe array[~np.isnan(intentions)] sert à
            # enlever les points où il n'y a pas de données présentes dans
            # les intentions
            jours_dispo = np.array(jours)[~np.isnan(intentions)]
            # Liste des durées disponibles.
            durees_dispo = np.array(durees)[~np.isnan(intentions)]
            # Liste des dates disponibles.
            dates_dispo = np.array(dates)[~np.isnan(intentions)]
            # Nuage de points des intentions de vote.
            points = ax.errorbar(dates_dispo, intentions[~np.isnan(intentions)],
                                 xerr=durees_dispo, yerr=confiances[~np.isnan(intentions)],
                                 fmt='+', color=couleur)
            # Ajout des errorbars, en les rendant invisibles.
            points.lines[0].set_visible(False)
            for err_line in points.lines[1]:
                err_line.set_visible(False)
            for err_line in points.lines[2]:
                err_line.set_visible(False)
            listePoints.append(points)

            if nom in exclus:
                continue

            # Fonction d’extrapolation.
            f = extrapole(w, durees, jours_dispo, intentions)

            # Abscisses pour tracer la fonction.
            x = np.linspace(jours_dispo[0], jours_dispo[-1], 1000)
            # Valeurs extrapolées (y) pour tracer la fonction.
            extrapolation = f.predict(x.reshape(-1, 1))
            # Discrétisation de l’ensemble des dates de sondage.
            date_disc = x.astype('timedelta64[h]') + debut
            # Erreurs constatées.
            # = Valeur de l’extrapolation aux points de sondage − vraies valeurs des intentions de vote à ce moment.
            e = np.abs(f.predict(jours_dispo.reshape(-1, 1)) - intentions[~np.isnan(intentions)])

            # Erreurs d’extrapolation.
            extrapolation_error_fn = interp1d(jours_dispo, e, kind='linear')
            erreurs = extrapolation_error_fn(x)
            # Valeurs corrigées (forcément entre 0 et 100).
            corrigees = np.clip(extrapolation, 0., 100.)
            # Borne inférieure de l’intervalle d’erreur.
            inferieur = np.maximum(corrigees - erreurs, 0.)
            # Borne supérieure de l’intervalle d’erreur.
            superieur = np.minimum(corrigees + erreurs, 100.)
            # Courbe de tendance.
            courbe, = ax.plot(date_disc, corrigees, c=couleur, label=nom)
            courbes.append(courbe)
            # Intervalles d’erreurs.
            intervalles_erreurs = ax.fill_between(date_disc, inferieur,
                                                  superieur, color=couleur,
                                                  alpha=.1)
            liste_erreurs.append(intervalles_erreurs)

    t_ech = t_ech[1:]
    print(f'Taille moyenne des échantillons : {statistics.mean(t_ech)}')
    print(f'Taille médiane des échantillons : {statistics.median(t_ech)}')
    print(f'Mode des tailles des échantillons : {statistics.mode(t_ech)}')
    #print(f'Variance des tailles des échantillons : {statistics.pvariance(t_ech)}')
    #print(f'Écart type des tailles des échantillons : {statistics.pstdev(t_ech)}')
    print(f'Écart type des tailles des échantillons : {math.sqrt(statistics.pvariance(t_ech))}')

    ax.set_title('Évolution temporelle des intentions de vote')
    ax.set_xlabel('Dates des sondages')
    ax.set_ylabel('Intentions de votes (en %)')
    # Pointeur vers la légende.
    legende = ax.legend(loc='best', fancybox=True)
    ax.grid(axis='x')
    ax.minorticks_on()
    ax.grid(axis='y', visible=True, which='major', linestyle='-', linewidth='1.5')
    ax.grid(axis='y', visible=True, which='minor', linestyle='--')
    # Zone où placer les cases à cocher.
    rax = plt.axes([0.9, 0.5, 0.2, 0.4])
    # Cases à cocher.
    cocher = CheckButtons(rax, candidats)

    for ligneLegend, ligneCourbe, ligneErreur in zip(legende.get_lines(), courbes, liste_erreurs):
        ligneLegend.set_linewidth(4.)
        ligneLegend.set_picker(True)
        ligneLegend.set_pickradius(8.)
        dicCourbes[ligneLegend] = ligneCourbe
        dicErreurs[ligneLegend] = ligneErreur

    fig.canvas.mpl_connect('pick_event', selectionne_ligne)
    cocher.on_clicked(selectionne_point)
    plt.show()


# Partie du code à exécuter au début du script.
if __name__ == '__main__':
    import argparse
    import sys

    # Analyseur de la ligne de commande.
    analyseur = argparse.ArgumentParser(
        description='Programme évaluant les tendances dans des sondages.')
    analyseur.add_argument('-v', '--version', action='version',
                           version='%(prog)s ' + __version__,
                           help='Affiche la version du programme et quitte.')
    analyseur.add_argument('-e', '--exclure', type=str,
                           help='Nom du fichier contenant les noms des '
                                'candidats à exclure de l’extrapolation '
                                'en raison d’un nombre de données trop '
                                'faible.')
    analyseur.add_argument('confiance', help='Définition de l’intervalle '
                                             'de confiance demandé (en '
                                             'pourcentage).')
    analyseur.add_argument('nom_fichier', help='Nom du fichier à analyser.')
    # Arguments de la ligne de commande.
    args = analyseur.parse_args()

    # Nom du fichier contenant les candidats à exclure de l’extrapolation
    # par manque de données.
    nomExclus = ''
    if args.exclure:
        nomExclus = args.exclure

    try:
        # Intervalle de confiance demandé.
        confiance = float(args.confiance)
    except ValueError as ex:
        print(f'Erreur 1 : impossible de convertir « {args.confiance} » en '
              'un nombre à virgule flottante . L’indicateur d’erreur est '
              f'« {ex} »',
              file=sys.stderr)
        sys.exit(1)

    try:
        traitement(confiance, args.nom_fichier, nomExclus)
    except ConfianceHorsLimites as ex:
        print('Erreur 2 : l’intervalle de confiance doit être strictement '
              'supérieur à 0 et strictement inférieur à 100',
              file=sys.stderr)
        sys.exit(2)
    except FichierInexistant as ex:
        print(f'Erreur 3 : pas de fichier nommé « {ex.nom_fichier} ».',
              file=sys.stderr)
        sys.exit(3)
    except JourInvalide as ex:
        print(f'Erreur 4 : dans le fichier « {ex.nom_fichier} », ligne '
              f'{ex.ligne}, la valeur {ex.colonne} est invalide. Elle doit '
              'être un jour codé au format ISO.', file=sys.stderr)
        sys.exit(4)
    except NbJoursInco as ex:
        print(f'Erreur 5 : dans le fichier « {ex.nom_fichier} », il n’y a '
              'pas le même nombre de dates de début de sondage que de dates '
              'de fin de sondage.', file=sys.stderr)
        sys.exit(5)
    except FinAvantDebut as ex:
        print(f'Erreur 6 : dans le fichier « {ex.nom_fichier} », colonne '
              f'{ex.colonne}, le sondage est indiqué comme finissant avant '
              'd’avoir commencé.', file=sys.stderr)
        sys.exit(6)
    except EchantillonInvalide as ex:
        print(f'Erreur 7 : dans le fichier « {ex.nom_fichier} », ligne '
              f'{ex.ligne}, la valeur {ex.colonne} est invalide : elle doit '
              'être un entier.', file=sys.stderr)
        sys.exit(7)
    except EchantillonHorsLimites as ex:
        print(f'Erreur 8 : dans le fichier « {ex.nom_fichier} », ligne '
              f'{ex.ligne}, la valeur {ex.colonne} doit être supérieure ou '
              'égale à 1.', file=sys.stderr)
        sys.exit(8)
    except CouleurInvalide as ex:
        print(f'Erreur 9 : dans le fichier « {ex.nom_fichier} », ligne '
              f'{ex.ligne}, la valeur {ex.colonne} ne correspond pas à une '
              'couleur disponible.', file=sys.stderr)
        sys.exit(9)
    except IntentionInval as ex:
        print(f'Erreur 10 : dans le fichier « {ex.nom_fichier} », pour le '
              f'candidat {ex.nom_candidat}, colonne {ex.colonne}, la valeur '
              f'a un format invalide ; elle doit être un nombre à virgule '
              'flottante.', file=sys.stderr)
        sys.exit(10)
    except IntentionHorsLimites as ex:
        print(f'Erreur 11 : dans le fichier « {ex.nom_fichier} », pour le '
              f'candidat {ex.nom_candidat}, colonne {ex.colonne}, la valeur '
              'est en-dehors des limites.', file=sys.stderr)
        sys.exit(11)
    except NbEchantillonsInco as ex:
        print(f'Erreur 12 : dans le fichier « {ex.nom_fichier} », il n’y a '
              'pas le même nombre de dates de sondage que de nombre '
              'd’échantillons.', file=sys.stderr)
        sys.exit(12)
