#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Programme simple calculant la matrice des corrélations d’une série de
sondages.

:module: calculMatCorr
:author: Yoann LE BARS

Pour le lancer, il suffit d’exécuter le script nommé « calculMatCorr.py »
situé dans le répertoire « src ». La syntaxe est la suivante :

::

    calculMatCorr.py [-h] [-v] nomFichierSondages nomFichierMatrice

Avec `nomFichierSondages` le chemin d’accès au fichier contenant les sondages
à analyser (il y en a de disponibles dans le répertoire `ressources`) et
`nomFichierMatrice` le fichier dans lequel stocker la matrice de corrélations.

L’option `-h` (ou `--help`) produit l’affichage d’un message d’aide et met
fin au programme, tandis que l’option `-v` (ou `--version`) produit
l’affichage de la version du programme et met fin à l’exécution.

Ce programme est sous contrat de distribution CeCILL v2.1. Vous pouvez le
modifier et le redistribuer, à la condition que vous en citiez l’auteur
original et que vous ne changez pas le contrat de distribution.

Le texte complet du contrat est disponible dans le fichier « LICENSE » et à
l’adresse suivante :

https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html
"""


from erreurs import *
from os import path
import csv
from utilitaires import format_corr
from constantes import __version__, negatives, listeReponses


def rempli_matrice(nom_fichier_sondages: os.PathLike, nom_fichier_matrice: str) -> None:
    """
    Rempli la matrice des corrélations pour un fichier de sondages donné.

    :param os.PathLike nom_fichier_sondages: Chemin d’accès vers le fichier
        contenant la série des sondages.
    :param str nom_fichier_matrice: Chemin d’accès vers le fichier où stocker
        la matrice de corrélations.

    :raises FichierInexistant: Si jamais le chemin d’accès au fichier des
        sondages est incorrect.
    """

    if (not path.exists(nom_fichier_sondages)) or (not path.isfile(nom_fichier_sondages)):
        raise FichierInexistant(nom_fichier_sondages)

    # Liste des candidats à l’élection.
    liste_candidats = []

    with open(nom_fichier_sondages, newline='', encoding='utf-8') as fichier:
        # Lecteur du fichier CSV.
        lecteur = csv.reader(fichier)

        liste_candidats = [line[0] for line in csv.reader(fichier.readlines())][3:]

    with open(nom_fichier_matrice, 'w', newline='', encoding='utf-8') as fichier:
        # Écrivain CSV.
        ecrivain = csv.writer(fichier)
        ecrivain.writerow([''] + liste_candidats)
        for candidat1 in liste_candidats:
            # Liste des corrélations.
            liste_corr = [candidat1]\
                         + [format_corr(nom_fichier_sondages, candidat1, candidat2) for candidat2 in liste_candidats]
            ecrivain.writerow(liste_corr)


# Partie du code à exécuter au début du script.
if __name__ == '__main__':
    import argparse
    import sys

    # Analyseur de la ligne de commande.
    analyseur = argparse.ArgumentParser(
        description='Programme déterminant la matrice des corrélations '
                    'entre candidats dans une série de sondages.')
    analyseur.add_argument('-v', '--version', action='version',
                           version='%(prog)s ' + __version__,
                           help='Affiche la version du programme et quitte.')
    analyseur.add_argument('nomFichierSondages',
                           help='Nom du fichier contenant les sondages.')
    analyseur.add_argument('nomFichierMatrice',
                           help='Nom du fichier dans lequel stocker la '
                                'matrice.')
    # Arguments de la ligne de commande.
    args = analyseur.parse_args()

    # Nom du fichier où stocker la matrice de corrélations.
    nom_fichier_matrice = args.nomFichierMatrice
    if path.exists(nom_fichier_matrice):
        if path.isfile(nom_fichier_matrice):
            # Réponse de l’utilisateur.
            reponse = input(f'Le fichier « {nom_fichier_matrice} » existe '
                            'déjà, voulez-vous l’écraser ([o]ui ou [n]on) ? ')
            while reponse not in listeReponses:
                print('Réponse non valide.')
                reponse = input(f'Le fichier « {nom_fichier_matrice} » '
                                'existe déjà, voulez-vous l’écraser ([o]ui '
                                'ou [n]on) ? ')
            if reponse in negatives:
                print('Entrez un autre nom pour le fichier de matrice de '
                      'corrélations.')
                sys.exit(-1)
        else:
            print(f'« {nom_fichier_matrice} » n’est pas un fichier.',
                  file=sys.stderr)
            sys.exit(-2)

    try:
        rempli_matrice(args.nomFichierSondages, nom_fichier_matrice)
    except FichierInexistant as ex:
        print(f'Pas de fichier nommé « {ex.nom_fichier} ».',
              file=sys.stderr)
        sys.exit(-3)
    except csv.Error as ex:
        print('Erreur pendant l’écriture du fichier {} : '
              '{}'.format(nomFichierMatrice, ex), file=sys.stderr)
        sys.exit(-4)
    except IntentionInvalide as ex:
        print(f'Erreur 5 : dans le fichier « {ex.nom_fichier} », ligne '
              f'{ex.ligne}, la valeur {ex.colonne} est invalide : elle doit '
              f'être un nombre à virgule flottante.', file=sys.stderr)
        sys.exit(5)
    except IntentionHorsLim as ex:
        print(f'Erreur 5 : dans le fichier « {ex.nom_fichier} », ligne '
              f'{ex.ligne}, la valeur {ex.colonne} doit être supérieure ou '
              f'égale à 0 et inférieure ou égale à 100.', file=sys.stderr)
        sys.exit(5)
    except NomInexistant as ex:
        print(f'Erreur 6 : pas de candidat nommé « {ex.nom} ».',
              file=sys.stderr)
        sys.exit(6)
