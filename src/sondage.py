#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Classes pour la gestion des sondages.

:module: sondage
:author: Anathexis
:author: Yoann LE BARS

Ce programme est sous contrat de distribution CeCILL v2.1. Vous pouvez le
modifier et le redistribuer, à la condition que vous en citiez l’auteur
original et que vous ne changez pas le contrat de distribution.

Le texte complet du contrat est disponible dans le fichier « LICENSE » et à
l’adresse suivante :

https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html
"""


import csv
from datetime import datetime, timedelta
from functools import lru_cache
import os
import numpy as np
from typing import Iterator, List
import logging
from sklearn.svm import SVR
import pandas as pd
from erreurs import *
import matplotlib.colors as mcolors
import typing
from constantes import nbSecondesDansJour


# Description à compléter.
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


# Déclaration anticipée de classes pour cause de dépendances récursives.
Sondage = typing.NewType("Sondage", None)
SerieIntention = typing.NewType("SerieIntention", None)


class ListeSondage:
    """
    Classe pour la gestion des sondages.

    :param List[Sondage] liste: Liste regroupant les différents sondages.
    :param List[SerieIntention] liste_serie_intention: Ensemble des intentions de votes (par candidat).
    """

    liste: List[Sondage]
    liste_serie_intention: List[SerieIntention]

    def __init__(self) -> None:
        """
        Initialisation de la classe.
        """

        # Description à compléter.
        self.liste = []
        # Série des intentions de votes.
        self.liste_serie_intention = []

    def get_sondage(self, index) -> 'Sondage':
        """
        Permet de récupérer un sondage en donnant son index dans la liste.

        :param index: Index du sondage à récupérer.

        :returns: Sondage à l'index demandé.
        :rtype: Sondage
        """

        return self.liste[index]

    def display_sondage(self, index, full_display: bool = False):
        """
        Méthode permettant d'afficher les différents scores des candidats dans un même sondage.

        :param index: Index du sondage à afficher.
        :param full_display: Détermine l'affichage des données vides : oui si
            true, non si false.
        """

        print(self.liste[index])
        print([(serie.nom_candidat, serie.serie_sondage[index])
               for serie in self.liste_serie_intention if (full_display or not np.isnan(serie.serie_sondage[index]))])

    def get_candidate_serie(self, candidate_name: str) -> 'SerieIntention':
        """
        Retourne la série de points de sondes du candidat demandé.

        :param str candidate_name: Nom du candidat à traiter.

        :returns: Série de points de sondes du candidat demandé.
        :rtype: SerieIntention
        """

        # Ensemble des points de sondage pour le candidat.
        ix = [serie.nom_candidat for serie in self.liste_serie_intention].index(candidate_name)
        return self.liste_serie_intention[ix]

    def get_durees(self) -> np.ndarray:
        """
        Renvoie la liste des durées des différents sondages (en secondes).

        :returns: Liste de durées des sondages (en secondes).
        :rtype: np.ndarray[np.timedelta64]
        """

        return np.array([sonde.duree for sonde in self.liste]).astype('timedelta64[s]')

    def get_start_dates(self) -> np.ndarray:
        """
        Renvoie la liste des dates de début des différents sondages.

        :returns: liste des dates de début des sondages.
        :rtype: np.ndarray[np.datetime64]
        """

        return np.array([sonde.date_debut for sonde in self.liste]).astype('datetime64[s]')

    @property
    @lru_cache
    def sample_size_array(self) -> np.ndarray:
        """
        Renvoie la liste des tailles d'échantillons des différents sondages.

        :returns: Liste des tailles d'échantillons des sondages.
        :rtype: np.ndarray[int]
        """

        return np.array([sondage.echantillon for sondage in self.liste])

    @classmethod
    def _traite_date(cls, chaine_valeur: str, nom_fichier_courant: os.PathLike,
                     ligne: int, colonne: int) -> datetime:
        """
        Convertit une chaîne de caractères en une date et renvoie une erreur
        en cas de format invalide.

        :param str chaine_valeur: La chaîne de caractères à convertir.
        :param os.PathLike nom_fichier_courant: Chemin d’accès au fichier
            contenant les intentions de votes.
        :param int ligne: Indice de la ligne courante.

        :returns: La date sous forme de `datetime`.

        :raises JourInvalide: Si jamais une date n’est pas renseignée sous
            forme d’une chaîne de caractères valide.
        """

        try:
            return datetime.strptime(chaine_valeur, '%Y-%m-%d')
        except ValueError:
            raise JourInvalide(nom_fichier_courant, ligne, colonne)

    @classmethod
    def create_sondage_list_from_csv_reader(cls, lecteur_courant: Iterator[List[str]],
                                            nom_fichier_courant: os.PathLike) -> 'ListeSondage':
        """
        Créé la liste de sondages à partir d’un reader de CSV formaté selon
        les spécifications données dans la documentation. S'invoque à partir
        de la classe, de cette façon:

        .. code-block:: python

            from sondage import ListeSondage
            with open(os.path.join('path','to','file'), newline = '', encoding = 'utf-8') as file:
                my_reader = csv.reader(fichier)
                ListeSondage.create_sondage_list_from_csv_reader(my_reader)

        :param Iterator[List[str]] lecteur_courant: Itérateur (pouvant par
            exemple être retourné lors de la lecture d’un fichier csv)
        :param os.PathLike nom_fichier_courant: Nom du fichier contenant les
            sondages.

        :returns: Une instance de la liste des sondages
        :rtype: ListeSondage

        :raises NbJoursInco: Si jamais il n’y a pas le même nombre de dates
            de début et de fin de sondage.
        :raises NbEchantillonsInco: Si jamais il n’y a pas le même nombre de
            dates de sondage que d’échantillons de sondés.
        :raises FinAvantDebut: Si jamais une date de fin de sondage est
            antérieure à la date de début du même sondage.
        """

        # Instanciation de la liste courante de sondages.
        current_sondage_list = cls()
        # La première ligne du fichier contient les dates de début de sondages.
        beginning_date_line = lecteur_courant.__next__()
        # La deuxième ligne contient les dates de fin de sondages.
        end_date_line = lecteur_courant.__next__()
        # La troisième ligne contient la taille des échantillons sondés.
        sample_size_line = lecteur_courant.__next__()
        # Vérification du format de données.
        if len(beginning_date_line) != len(end_date_line):
            raise NbJoursInco(nom_fichier_courant)
        if len(end_date_line) != len(sample_size_line):
            raise NbEchantillonsInco(nom_fichier_courant)

        # -- Création des sondages
        # Liste des jours sous forme de chaînes de caractères.
        liste_jours = beginning_date_line[2:]
        # Liste des jours de début des sondages.
        # timedelta(hours=8) parce qu’on fixe l’heure de début à 8 h.
        jours_debut = np.array([cls._traite_date(liste_jours[i], nom_fichier_courant, 1, i + 3)
                                + timedelta(hours=8) for i in range(len(liste_jours))])
        liste_jours = end_date_line[2:]
        # Liste des jours de fin des sondages.
        # timedelta(hours=20) parce qu’on fixe l’heure de fin à 20 h.
        jours_fin = np.array([cls._traite_date(liste_jours[i], nom_fichier_courant, 2, i + 3)
                              + timedelta(hours=20) for i in range(len(liste_jours))])
        print('Lecture des sondages.')
        for index in range(2, len(beginning_date_line)):
            # Date de début du sondage courant.
            debut_sondage = jours_debut[index - 2]
            # Date de fin du sondage courant.
            fin_sondage = jours_fin[index - 2]
            if fin_sondage <= debut_sondage:
                raise FinAvantDebut(nom_fichier_courant, index + 1)
            try:
                # Sondage en cours de traitement.
                current_sounding = Sondage(date_debut=debut_sondage, date_fin=fin_sondage,
                                           echantillon=int(sample_size_line[index]), liste_sondage=current_sondage_list,
                                           nom_fichier_courant=nom_fichier_courant, index=index)
            except ValueError:
                raise EchantillonInvalide(nom_fichier_courant, 3, index + 1)
            current_sondage_list.liste.append(current_sounding)
        
        # -- Création des séries d’intentions par candidat
        for csv_line in lecteur_courant:
            print(f'Traitement du candidat {csv_line[0]}.')
            # Série en cours de traitement.
            current_serie = SerieIntention.create_serie_from_csv_line(nom_fichier_courant, csv_line,
                                                                      current_sondage_list)
            current_sondage_list.liste_serie_intention.append(current_serie)
        
        return current_sondage_list

    def create_dataframe_from(self) -> pd.DataFrame:
        """
        Méthode pour créer un dataframe reprenant les données de chaque point
        de sondage de façon dénormalisée. Le dataframe a six colonnes :

        - sondage_index (int) : l'index du sondage dans la liste ;
        - sondage_start_date (date) : la date de début du sondage ;
        - sondage_end_date (date) : la date de fin du sondage ;
        - sondage_sample_size (int) : la taille de l’échantillon sondé par le sondage ;
        - candidate_name (str) : le nom du candidat sondé ;
        - candidate_sondage_score (float) : le score du candidat dans le sondage associé.

        La combinaison sondage_index et candidate_name est une clé unique.

        :returns: Dataframe dénormalisé reprenant les données de chaque point
            de sondage, y inclus nans là où le candidat n’a pas été testé.
        :rtype: pd.DataFrame
        """

        # Ensemble des points de sondage.
        sondage_points_tuple_list = []
        
        for serie in self.liste_serie_intention:
            # Ensemble des points de sondage associés au candidat.
            candidate_sounding_point_list = map(lambda x: dict(candidate_name=serie.nom_candidat,
                                                               candidate_sondage_score=x), serie.serie_sondage)
            sondage_points_tuple_list.extend(list(zip([
                dict(
                    sondage_index=self.liste.index(sondage),
                    sondage_start_date=sondage.date_debut,
                    sondage_end_date=sondage.date_fin,
                    sondage_sample_size=sondage.echantillon
                ) for sondage in self.liste if self.liste.index(sondage)], candidate_sounding_point_list)))

        return pd.DataFrame.from_records(list(map(lambda x: dict(**x[0], **x[1]), sondage_points_tuple_list)))


class SerieIntention:
    """
    Classe décrivant les séries de point de sondages d'un candidat.

    :param str nom_candidat: Nom du candidat à traiter.
    :param str couleur_candidat: Couleur associée au candidat.
    :param os.PathLike _nomFicher: Chemin d’accès vers le fichier contenant
        les sondages.
    :param np.ndarray serie_sondage: Liste des intentions de votes associées
        au candidat.
    :param ListeSondage liste_sondage: Référence à la liste de sondages
        associée afin de permettre un traitement plus aisé.
    :param np.array available_days: Liste des jours de sondages.
    """

    nom_candidat: str
    couleur_candidat: str
    _nom_fichier: os.PathLike
    serie_sondage: np.ndarray
    liste_sondage: ListeSondage
    available_days: np.array

    def __init__(self, nom_candidat: str, couleur_candidat: str,
                 nom_fichier_courant: os.PathLike, serie_sondage: np.ndarray,
                 liste_sondage: ListeSondage) -> None:
        """
        Initialisation de la classe.

        :param str nom_candidat: Nom du candidat à traiter.
        :param str couleur_candidat: Couleur associée au candidat.
        :param os.PathLike nom_fichier_courant: Chemin d’accès vers le
            fichier contenant les sondages.
        :param np.ndarray serie_sondage: Série des dates de sondage.
        :param ListeSondage liste_sondage: Liste des intentions de votes
            envers chaque candidat.

        :raises NbIntentionsInco: Si jamais la série d’intentions de votes
            envers un candidat est de taille différente des autres séries.
        """

        self.nom_candidat = nom_candidat
        self.couleurCandidat = couleur_candidat
        self._nom_fichier = nom_fichier_courant
        self.serie_sondage = serie_sondage
        self.liste_sondage = liste_sondage
        if len(serie_sondage) != len(liste_sondage.liste):
            raise NbIntentionsInco(nom_fichier_courant, self.nom_candidat, len(serie_sondage), len(liste_sondage.liste))
        self.available_days =\
            np.array([sondage.day for sondage in
                      self.liste_sondage.liste])[~np.isnan(self.serie_sondage)].astype('datetime64[s]').astype('float64') / nbSecondesDansJour

    @classmethod
    def _traite_intention(cls, texte_valeur: str, nom_fichier_courant: os.PathLike,
                          nom_candidat: str, colonne: int) -> np.float:
        """
        Convertit une chaîne de caractères en intention de vote et renvoie une
        erreur en cas de format invalide.

        :param str texte_valeur: La chaîne de caractères à convertir.
        :param os.PathLike nom_fichier_courant: Chemin d’accès au fichier
            contenant les intentions de votes.
        :param str nom_candidat: Nom du candidat courant.

        :returns: La valeur sous forme de nombre à virgule flottante.

        :raises EchantillonInvalideV2: Si jamais une intention de vote n’est
            pas renseignée sour forme d’une chaîne de caractères valide.
        """

        if texte_valeur == '':
            return np.nan
        else:
            try:
                return float(texte_valeur)
            except ValueError:
                raise EchantillonInval(nom_fichier_courant, nom_candidat, colonne)

    @classmethod
    def create_serie_from_csv_line(cls, nom_fichier_courant: os.PathLike, csv_line: List[str],
                                   liste_sondage: ListeSondage) -> 'SerieIntention':
        """
        Créé la série de points de sondages à partir d’une ligne d’un reader
        de CSV formaté selon les spécifications données dans la
        documentation. En pratique s'utilise rarement seul.

        :param os.PathLike nom_fichier_courant: Chemin d’accès vers le
            fichier contenant les sondages.
        :param List[str] csv_line: Liste stockant le contenu de la ligne
            courante du fichier CSV.
        :param ListeSondage liste_sondage: Description à compléter.

        :returns: La série de points de sonde liée au candidat.
        :rtype: SerieIntention

        :raises CouleurInvalideV2: Si jamais un nom de couleur invalide est
            renseigné.
        """

        # Nom du candidat courant.
        nom_candidat = csv_line[0]
        # Couleur associée au candidat courant.
        couleur_candidat = csv_line[1]
        # Liste des couleurs possible.
        couleurs_possibles = [*mcolors.BASE_COLORS] + [*mcolors.TABLEAU_COLORS] + [*mcolors.CSS4_COLORS]
        if couleur_candidat not in couleurs_possibles:
            raise CouleurInval(nom_fichier_courant, nom_candidat, 2)
        # Liste des intentions de votes sous forme de chaînes de caractères.
        intentions = csv_line[2:]
        # Liste des intentions de votes en faveur du candidat.
        #serie_intentions = np.array([float(intention) if re.match(r'^-?\d+(?:\.\d+)?$', intention) is not None else np.nan for intention in csv_line[2:]])
        serie_intentions = np.array([cls._traite_intention(intentions[i], nom_fichier_courant, nom_candidat, i + 3)
                                     for i in range(len(intentions))])
        cls.sanity_checks_serie(nom_fichier_courant, nom_candidat, serie_intentions)
        return cls(
            nom_candidat=nom_candidat,
            couleur_candidat=mcolors.to_hex(couleur_candidat),
            nom_fichier_courant=nom_fichier_courant,
            serie_sondage=serie_intentions,
            liste_sondage=liste_sondage
        )

    @classmethod
    def sanity_checks_serie(cls, nom_fichier_courant: os.PathLike,
                            nom_candidat: str, serie: np.ndarray,
                            bounds: List[float] = [0., 100.]) -> np.ndarray:
        """
        Méthode permettant de s'assurer que les points de sonde sont 
        inclus dans un intervalle donné, par défaut [0, 100].

        :param nom_fichier_courant: Chemin d’accès au fichier contenant les sondages.
        :param nom_candidat: Nom du candidat courant.
        :param np.ndarray serie: Série d’intentions de votes à valider.
        :param List[float] bounds: Bornes minimum et maximum.

        :returns: La série si pas d'erreur, une exception sinon.
        :rtype: np.ndarray[float]

        :raises: IntentionHorsLimites si une valeur est en dehors de
            l’intervalle.
        """

        print(f'Vérification des intentions de votes envers {nom_candidat}.')
        # Colonne de la valeur courante.
        colonne = 3
        for valeur in serie:
            if ~np.isnan(valeur) and (valeur < bounds[0] or valeur > bounds[1]):
                raise IntentionHorsLimites(nom_fichier_courant, nom_candidat, colonne)
            colonne += 1
        return serie

    @property
    def normalised_serie(self) -> np.ndarray:
        """
        Convertie une série en pourcentage en une série normalisée dans
        [1 ; 0].

        :returns: La série normalisée.
        :rtype: np.ndarray[float]
        """

        return self.serie_sondage / 100.

    @property
    def s(self) -> np.ndarray:
        """
        Calcul la correction de l’intervalle de confiance.

        :returns: La liste des corrections.
        :rtype: np.ndarray[float]
        """

        # Liste des corrections.
        s = np.empty_like(self.normalised_serie)
        # Liste des index aux limites.
        bounds_idx = np.logical_or(self.normalised_serie == 1., self.normalised_serie == 0.)
        s[bounds_idx] = 1.e-7
        s[~bounds_idx] = np.power(
            (self.normalised_serie[~bounds_idx] * (1. - self.normalised_serie[~bounds_idx]))
            /
            self.liste_sondage.sample_size_array[~bounds_idx],
            0.5)

        return s

    @property
    def w(self) -> np.ndarray:
        """
        Calcul du poids des sondages.

        :returns: La liste des poids.
        :rtype: np.ndarray[float]
        """

        w = np.power(self.s, -1)
        return w

    def get_extrapolation(self):
        """
        Renvoie l’extrapolation des sondages.

        :returns: Un polynôme permettant l'extrapolation.
        """

        # Liste des poids des sondages.
        poids = self.w[~np.isnan(self.serie_sondage)] + (nbSecondesDansJour / np.array(self.liste_sondage.get_durees().astype('float64')))[~np.isnan(self.serie_sondage)].astype('float64')
        # Système d’extrapolation.
        svr = SVR(kernel='rbf', gamma='scale', C=1.0, epsilon=0.1)
        return svr.fit(self.available_days.reshape(-1, 1), self.serie_sondage[~np.isnan(self.serie_sondage)], poids)

    def get_confidence_array(self, q: float) -> np.ndarray:
        """
        Calcul les intervalles de confiance.

        :param float q: Paramètre de correction de l’intervalle de confiance.

        :returns: Array des intervalles de confiance.
        :rtype: np.ndarray[float]
        """

        return q * self.s * 100.

    def get_max(self) -> float:
        return max(self.serie_sondage)

    def get_min(self) -> float:
        return min(self.serie_sondage)

    def __str__(self):
        """
        Représentation textuelle de la classe

        :returns: String représentant la classe.
        :rtype: str
        """

        return f'Série de {self.nom_candidat}, {self.serie_sondage}'
        

class Sondage:
    """
    Classe pour la gestion des sondages.

    :param datetime date_debut: Date de début du sondage.
    :param datetime date_fin: Date de fin du sondage.
    :param int echantillon: Taille de l’échantillon.
    :param ListeSondage liste_sondage: Liste des intentions de votes.
    """

    def __init__(self, date_debut: datetime, date_fin: datetime,
                 echantillon: int, liste_sondage: ListeSondage,
                 nom_fichier_courant: os.PathLike, index: int) -> None:
        """
        Initialisation de la classe.

        :param datetime date_debut: Date de début du sondage.
        :param datetime date_fin: Date de fin du sondage.
        :param int echantillon: Taille de l’échantillon.
        :param ListeSondage liste_sondage: Liste des intentions de votes.
        :param os.PathLike nom_fichier_courant: Chemin d’accès au fichier
            contenant les sondages.
        :param int index: Position de l’échantillon en cours de traitement
            dans la liste.
        """

        if echantillon < 1:
            raise EchantillonHorsLimites(nom_fichier_courant, 3, index + 1)

        self.date_debut = date_debut
        self.date_fin = date_fin
        self.echantillon = echantillon
        self.listeSondage = liste_sondage

    def __str__(self) -> str:
        """
        Représentation textuelle de la classe

        :returns: String représentant la classe.
        :rtype: str
        """

        return f'Sondage {self.listeSondage.liste.index(self)}, du '\
               f'{self.date_debut} au {self.date_fin} ({self.echantillon} pers.)'

    @property
    def duree(self):
        """
        Retourne la durée du sondage.

        :returns: Durée du sondage.
        :rtype: timedelta
        """

        return self.date_fin - self.date_debut

    @property
    def day(self):
        """
        Retourne la date de milieu de sondage, considérée comme point
        d’extrapolation.

        :returns: Date du milieu de sondage.
        :rtype: datetime
        """

        return self.date_debut + (self.duree / 2)

    def __repr__(self) -> str:
        """
        Représentation textuelle de la classe

        :returns: String représentant la classe.
        :rtype: str
        """

        return f'Sondage {self.listeSondage.liste.index(self)}, du '\
               f'{datetime.date(self.date_debut)} au '\
               f'{datetime.date(self.date_fin)} ({self.echantillon} pers.)'


# Partie du code à exécuter au début du script.
if __name__ == '__main__':
    # Nom du fichier à ouvrir.
    nom_fichier = os.path.join('ressources', 'presidentielle2022-tour1.csv')
    with open(nom_fichier, newline='', encoding='utf-8') as fichier:
        # Lecteur du fichier CSV.
        lecteur = csv.reader(fichier)
        # Description à compléter.
        sdg_lst = ListeSondage.create_sondage_list_from_csv_reader(lecteur, os.PathLike[nom_fichier])
        
        my_sondage_df = sdg_lst.create_dataframe_from()
        print(my_sondage_df)
