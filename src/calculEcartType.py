#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Programme simple calculant l’écart type d’une série de sondages concernant un
candidat.

:module: calculMatCorr
:author: Yoann LE BARS

Pour le lancer, il suffit d’exécuter le script nommé « calculEcartType.py »
situé dans le répertoire « src ». La syntaxe est la suivante :

::

    calculEcartType.py [-h] [-v] nomFichierSondages nomFichierCandidat

Avec `nomFichierSondages` le chemin d’accès au fichier contenant les sondages
à analyser (il y en a de disponibles dans le répertoire `ressources`) et
`nomFichierCandidat` le chemin d’accès vers un fichier contenant le nom du
candidat à analyser.

L’option `-h` (ou `--help`) produit l’affichage d’un message d’aide et met
fin au programme, tandis que l’option `-v` (ou `--version`) produit
l’affichage de la version du programme et met fin à l’exécution.

Ce programme est sous contrat de distribution CeCILL v2.1. Vous pouvez le
modifier et le redistribuer, à la condition que vous en citiez l’auteur
original et que vous ne changez pas le contrat de distribution.

Le texte complet du contrat est disponible dans le fichier « LICENSE » et à
l’adresse suivante :

https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html
"""


from erreurs import *
from os import path
import csv
import statistics as st
from constantes import __version__


def traitement(nom_fichier_sondages: os.PathLike, nom_candidat: str) -> float:
    """
    Calcul l’écart type des intentions de votes envers un candidat.

    :param os.PathLike nom_fichier_sondages: Chemin d’accès vers le fichier
        contenant les sondages.
    :param str nom_candidat: Nom du candidat à analyser.

    :returns: L’écart type des intentions de votes.
    :rtype: float

    :raises FichierInexistant: Si jamais le chemin d’accès au fichier des
        sondages est incorrect.
    :raises IntentionInval: Si jamais une intention de vote n’est pas
        exprimée sous forme de nombre à virgule flottante.
    :raises IntentionHorsLimites: Si jamais une intention de vote est
        inférieure à 0 % ou supérieure à 100 %.
    :raises NomInexistant: Si jamais le nom de candidat donné est introuvable.
    """

    if (not path.exists(nom_fichier_sondages)) or (not path.isfile(nom_fichier_sondages)):
        raise FichierInexistant(nom_fichier_sondages)

    # Série des intentions de votes envers le candidat sous forme de chaînes de caractères.
    intentions = []
    # Indique si oui ou non le nom a été trouvé.
    trouve_nom = False
    with open(nom_fichier_sondages, newline='', encoding='utf8') as fichier_courant:
        # Lecteur du fichier CSV.
        lecteur_courant = csv.reader(fichier_courant)
        for ligne in lecteur_courant:
            if ligne[0] in nom_candidat:
                intentions = ligne[2:]
                trouve_nom = True
    if not trouve_nom:
        raise NomInexistant(nom_candidat)

    print(f'{nom_candidat}')

    # Série des intentions de votes sous forme de flottants.
    sondages = []
    for i in range(len(intentions)):
        # Valeur courante à traiter.
        x = intentions[i]
        if x != '':
            try:
                # Intention de vote courante.
                valeur_x = float(x)
            except ValueError:
                raise IntentionInval(nom_fichier_sondages, nom_candidat, i + 3)
            if valeur_x < 0. or valeur_x > 100.:
                raise IntentionHorsLimites(nom_fichier_sondages, nom_candidat, i + 3)
            sondages.append(valeur_x)

    return st.pstdev(sondages)


# Partie du code à exécuter au début du script.
if __name__ == '__main__':
    import argparse
    import sys

    # Analyseur de la ligne de commande.
    analyseur = argparse.ArgumentParser(
        description='Programme calculant l’écart type d’une série de '
                    'sondages concernant un candidats.')
    analyseur.add_argument('-v', '--version', action='version',
                           version='%(prog)s ' + __version__,
                           help='Affiche la version du programme et quitte.')
    analyseur.add_argument('nom_fichier_sondages',
                           help='Nom du fichier contenant les sondages.')
    analyseur.add_argument('nom_fichier_selection',
                           help='Nom du fichier contenant le nom du '
                                'candidat à analyser.')
    # Arguments de la ligne de commande.
    args = analyseur.parse_args()

    # Nom du fichier contenant le candidat à analyser.
    nom_fichier_selection = args.nom_fichier_selection
    if (not path.exists(nom_fichier_selection)) or (not path.isfile(nom_fichier_selection)):
        print(f'Erreur 1 : pas de fichier nommé '
              f'« {nom_fichier_selection} ».', file=sys.stderr)
        sys.exit(1)
    with open(nom_fichier_selection, newline='', encoding='utf-8') as fichier:
        # Nom du candidat à analyser.
        nom = fichier.readline()

    try:
        print(traitement(args.nom_fichier_sondages, nom))
    except FichierInexistant as ex:
        print(f'Erreur 2 : pas de fichier nommé « {ex.nom_fichier} ».',
              file=sys.stderr)
        sys.exit(2)
    except IntentionInval as ex:
        print(f'Erreur 3 : dans le fichier « {ex.nom_fichier} », pour le '
              f'candidat {ex.nom_candidat}, colonne {ex.colonne}, la valeur '
              f'a un format invalide ; elle doit être un nombre à virgule '
              'flottante.', file=sys.stderr)
        sys.exit(3)
    except IntentionHorsLimites as ex:
        print(f'Erreur 4 : dans le fichier « {ex.nom_fichier} », pour le '
             f'candidat {ex.nom_candidat}, la valeur {ex.colonne} doit '
             f'être supérieure ou égale à 0 et inférieure ou égale à 100.',
             file=sys.stderr)
        sys.exit(4)
    except NomInexistant as ex:
        print(f'Erreur 5 : pas de candidat nommé « {ex.nom} ».',
              file=sys.stderr)
        sys.exit(5)
