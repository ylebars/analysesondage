#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Application pour évaluer les tendances dans un sondage d’opinion.

:module: app
:author: Anathexis
:author: Yoann LE BARS

Pour le lancer, il suffit d’exécuter le script nommé « app.py »
situé dans le répertoire « src ». La syntaxe est la suivante :

::

    app.py [-h] [-v] [-e nomExclus] confiance nom_fichier

Avec `confiance` la taille de l’intervalle de confiance en pourcentage (la
norme dans les sondages d’opinions est de regarder l’intervalle de confiance
à 95 %) et `nom_fichier` le chemin d’accès au fichier contenant les sondages à
analyser (il y en a de disponibles dans le répertoire `ressources`).

L’option `-h` (ou `--help`) produit l’affichage d’un message d’aide puis met
fin au programme, tandis que l’option `-v` (ou `--version`) produit
l’affichage de la version du programme puis met fin à l’exécution et l’option
`-e` (ou `--exclure`) permet de transmettre un fichier au format CSV dans
lequel chaque valeur est le nom d’un candidat à exclure de l’extrapolation
par faute d’un nombre suffisant de données.

En cliquant sur une ligne de la légende, on fait apparaître ou disparaître
l’extrapolation pour le candidat associé.

Des informations concernant les intervalles de confiance :

https://fr.wikipedia.org/wiki/Intervalle_de_confiance

Ce programme est sous contrat de distribution CeCILL v2.1. Vous pouvez le
modifier et le redistribuer, à la condition que vous en citiez l’auteur
original et que vous ne changez pas le contrat de distribution.

Le texte complet du contrat est disponible dans le fichier « LICENSE » et à
l’adresse suivante :

https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html
"""


import csv
from typing import Dict, List, Union
import dash
from dash import html
from dash import dcc
import numpy as np
import plotly.graph_objects as go
import os
from plotly import colors
from scipy.interpolate import interp1d
from erreurs import *
import math
from scipy.special import erfinv
from constantes import __version__

from sondage import ListeSondage


app = dash.Dash(__name__)


class SondageGraph:
    """
    Classe pour tracer le graphe des tendances.

    Le graphe est tracé en utilisant plotly, via deux attributs qui seront 
    traduits en json pour être interprétés par plotly.js. Les figures plotly
    sont composées de deux ensembles :
    - `data` : une liste de traces à placer (ce sont les données affichées)
    - `layout` : les informations sur le layout de la figure

    Ici, `data` est une liste de dictionnaires dont les clés sont les clés
    acceptées par plotly (cf. la documentation de plotly). Layout est un
    dictionnaire de clés, elles aussi acceptées par plotly ; plus d'infos
    dans la documentation.

    :param os.PathLike _file_name: Chemin d’accès au fichier contenant la liste
        des sondages.
    :param ListeSondage ls: Liste des intentions de votes.
    :param List[str] _candidate_list: Liste des candidats à l’élection.
    :param dict _layout: Dictionnaire reprenant les infos liées au layout du graphe
    :param List[dict] _data: Liste des traces à afficher
    :param go.Figure _figure: Pointeur vers le graphe.
    :param float _confiance: Valeur de l’intervalle de confiance.
    """

    _file_name: os.PathLike
    ls: ListeSondage
    _candidate_list: List[str]
    _layout: dict
    _data: List[dict]
    _figure: go.Figure
    _confiance: float

    def __init__(self, file_name: os.PathLike, confiance_demandee: float) -> None:
        """
        Initialisation de la classe.
        
        :param os.PathLike file_name: Chemin d’accès au fichier contenant la
            liste des sondages.
        :param float confiance_demandee: Valeur de l’intervalle de confiance.
            C’est un pourcentage, donc compris entre 0 et 100 exclus. Le code
            calcul l’intervalle pour lequel la probabilité que l’intention de
            votes soit bien inclue dans cet intervalle est cette valeur.
        """

        self._file_name = file_name
        self.ls = self.get_intents_from_file(file_name)
        self._candidate_list = self.get_candidate_list()
        self._layout = dict()
        self._data = []
        self._figure = go.Figure()
        self._confiance = confiance_demandee

    @property
    def data(self):
        """
        Accès à la liste des données.

        :returns: La liste de données.
        :rtype:
        """

        return self._data

    @property
    def layout(self) -> Dict:
        """
        Accès au layout du graphe.

        :returns: Un pointeur vers le layout du graphe.
        :rtype: Dict
        """

        return self._layout

    def get_intents_from_file(self, file_name: os.PathLike) -> ListeSondage:
        """
        Récupère les intentions de votes dans le fichier.

        :param os.PathLike file_name: Chemin d’accès à la base de données
            des votes.

        :returns: La liste des sondages.
        :rtype: ListeSondage
        """

        with open(file_name, newline='', encoding='utf-8', mode='r') as f:
            # Lecteur du fichier CSV.
            lecteur = csv.reader(f)
            return ListeSondage.create_sondage_list_from_csv_reader(lecteur, self._file_name)

    def get_candidate_list(self) -> List:
        """
        Accès à la liste des candidats.

        :returns: La liste des candidats.
        :rtype: List[str]
        """

        return [serie.nom_candidat for serie in self.ls.liste_serie_intention]

    def initialize_layout_dict(self, layout_dict: Union[dict, None] = None):
        """
        Créé le dictionnaire pour accéder aux éléments du layout du graphe.

        :param Union[dict, None] layout_dict: Dictionnaire à remplir.
        """

        # Get the upper bound of the y values (* 1.10 to add a bit of room)
        max_y_value = max([serie.get_max() for serie in self.ls.liste_serie_intention]) * 1.10

        if layout_dict is None:
            self._layout = {
                'grid': {
                    'rows': 2,
                    'columns': 1
                },
                'yaxis': {'range': [-5, max_y_value], 'anchor': 'y1', 'domain': [0.2, 1]},
                'yaxis2': {'anchor': 'y2', 'domain': [0, 0.15], 'visible': False},
                'yaxis3': {'range': [-5, max_y_value], 'anchor': 'y3', 'domain': [0.2, 1], 'visible': False},
            }
        else:
            self._layout = layout_dict

    def get_scatter_dict_from_candidate_serie(self, candidate_name: str) -> dict:
        """
        Méthode permettant de générer un dictionnaire représentant la trace des points
        de sondage d’un candidat.

        :param str candidate_name: Nom du candidat à traiter.

        :returns: Dictionnaire contenant les informations permettant d’afficher la
            trace d'un candidat.
        :rtype: dict
        """

        # Série à traiter
        current_serie = self.ls.get_candidate_serie(candidate_name)
        # Filtrage des nans
        filled_ix = ~np.isnan(current_serie.serie_sondage)
        # Création des tuples de couleurs pour jouer sur l’opacité
        rgb_color = colors.hex_to_rgb(current_serie.couleurCandidat)
        # Coefficient pour le calcul de l’intervalle de confiance.
        q = math.sqrt(2.) * erfinv(self._confiance)
        # Création du dictionnaire contenant les données de la trace
        scatter_dict = dict(
            y=current_serie.serie_sondage[filled_ix].astype(float),
            x=self.ls.get_start_dates()[filled_ix] + (self.ls.get_durees()[filled_ix] / 2.),
            error_y={'array': current_serie.get_confidence_array(q)[filled_ix],
                     'color': f'rgba({rgb_color[0]},{rgb_color[1]},{rgb_color[2]},0.1)'},
            xaxis='x1', yaxis='y1',
            marker={'color': current_serie.couleurCandidat, 'size': 4, 'opacity': 0.7},
            name=candidate_name,
            mode='markers'
        )
        return scatter_dict

    def get_curve_dict_from_candidate_serie(self, candidate_name: str) -> dict:
        """
        Méthode permettant de générer un dictionnaire représentant la trace
        de l’interpolation des points de sondage d’un candidat.

        :param str candidate_name: Nom du candidat à traiter.

        :returns: Dictionnaire représentant la trace de l’interpolation
            des points de sondage d’un candidat.
        :rtype: dict
        """

        # Série à traiter.
        current_serie = self.ls.get_candidate_serie(candidate_name)
        # Fonction d’extrapolation.
        f = current_serie.get_extrapolation()
        # Abscisses pour tracer la fonction.
        x = np.linspace(current_serie.available_days[0], current_serie.available_days[-1], 1000)
        # Valeurs extrapolées (y) pour tracer la fonction.
        extrapolation = f.predict(x.reshape(-1, 1))
        # Création du dictionnaire contenant les données de la trace
        scatter_dict = dict(
            # Ordonnées de la courbe.
            y=np.clip(extrapolation, 0, 100),
            x=(x * 24.).astype('datetime64[h]'),
            xaxis='x1', yaxis='y1',
            line={'color': current_serie.couleurCandidat, 'width': 2, 'smoothing': 0, 'shape': 'spline'},
            name=f'{candidate_name}',
            mode='lines'
        )
        return scatter_dict

    def get_extrapolation_error_curve_dict_from_candidate_serie(self, candidate_name: str) -> list[dict]:
        """
        Méthode permettant de générer un dictionnaire représentant la trace de l'erreur 
        d'interpolation des points de sondage d'un candidat.

        :param str candidate_name: Nom du candidat à traiter.

        :returns: Dictionnaire représentant la trace de l’erreur
            d’interpolation des points de sondage d’un candidat.
        :rtype: list[dict]
        """

        # Série à traiter.
        current_serie = self.ls.get_candidate_serie(candidate_name)
        # Couleur associée au candidat.
        rgb_color = colors.hex_to_rgb(current_serie.couleurCandidat)
        # Fonction d’extrapolation.
        f = current_serie.get_extrapolation()
        # Abscisses pour tracer la fonction.
        x = np.linspace(current_serie.available_days[0], current_serie.available_days[-1], 1000)
        # Valeurs extrapolées (y) pour tracer la fonction.
        extrapolation = f.predict(x.reshape(-1, 1))
        # Erreur par rapport aux sondages.
        e = np.abs(f.predict(current_serie.available_days.reshape(-1, 1))
                   - current_serie.serie_sondage[~np.isnan(current_serie.serie_sondage)])
        # Fonction d’erreur.
        extrapolation_error_fn = interp1d(current_serie.available_days, e, kind='linear')
        # Liste des erreurs.
        erreurs = extrapolation_error_fn(x)
        # Création du dictionnaire contenant les données de la trace des erreurs.
        error_traces = [
            dict(
                y=np.clip(extrapolation + erreurs, 0, 100),
                x=(x * 24.).astype('datetime64[h]'),
                xaxis='x1', yaxis='y1',
                mode='none', fill='tonexty',
                fillcolor=f'rgba({rgb_color[0]},{rgb_color[1]},{rgb_color[2]},0.0)',
                showlegend=False
            ),
            dict(
                y=np.clip(extrapolation - erreurs, 0, 100),
                x=(x * 24).astype('datetime64[h]'),
                xaxis='x1', yaxis='y1',
                name=f'{candidate_name} (Erreur)',
                mode='none',
                fill='tonexty',
                fillcolor=f'rgba({rgb_color[0]},{rgb_color[1]},{rgb_color[2]},0.1)'
            )
        ]
        return error_traces

    def get_timeline_dict_from_sondage_list(self):
        """
        Méthode permettant de générer un dictionnaire représentant la trace des
        intervalles de temps des différents sondages (par défaut en dessous des
        courbes d'extrapolation)

        :returns: dictionnaire représentant la trace des intervalles de temps
            des différents sondages
        :rtype: dict
        """

        tl_dict = dict(
            type='bar',
            orientation='v',
            width=(self.ls.get_durees().astype('int64')) * 1000.,
            x=self.ls.get_start_dates() + self.ls.get_durees() / 2.,
            y=np.full(len(self.ls.get_start_dates()), 0.5),
            base=np.tile([-1, -2, -3, -4, -5], 1 + int(len(self.ls.liste) / 5.))[0:len(self.ls.liste)],
            yaxis='y2',
            marker=dict(color='rgb(0,0,0)', opacity=0.2),
            name='Durée des sondages',
            hovertemplate=[str(sonde) for sonde in self.ls.liste]
        )
        return tl_dict

    def create_data_dict_list_for_scatter_plot(self):
        """
        Méthode permettant de générer toutes les traces nécessaires
        aux différents graphes (points, interpolation, erreurs)
        """

        self.initialize_layout_dict()
        for candidate_name in self.get_candidate_list():
            # Erreurs sur l'interpolation
            self._data.extend(self.get_extrapolation_error_curve_dict_from_candidate_serie(candidate_name))
            # Courbes d'interpolation
            self._data.append(self.get_curve_dict_from_candidate_serie(candidate_name=candidate_name))
            # Points de sondages
            self._data.append(self.get_scatter_dict_from_candidate_serie(candidate_name))
        # Intervalles de temps des sondages
        self._data.append(self.get_timeline_dict_from_sondage_list())

    def create_fig_from_data_layout(self):
        """
        Création de la figure en elle-même

        :returns: Pointeur vers la figure.
        :rtype:
        """

        self.create_data_dict_list_for_scatter_plot()
        # Pointeur vers la figure.
        fig = go.Figure(
            layout=self.layout,
            data=self.data
        )
        return fig


# Partie du code à exécuter au début du script.
if __name__ == "__main__":
    import argparse
    import sys
    import pathlib

    # Analyseur de la ligne de commande.
    analyseur = argparse.ArgumentParser(
        description='Programme évaluant les tendances dans des sondages.')
    analyseur.add_argument('-v', '--version', action='version',
                           version='%(prog)s ' + __version__,
                           help='Affiche la version du programme et quitte.')
    analyseur.add_argument('-e', '--exclure', type=str,
                           help='Nom du fichier contenant les noms des '
                                'candidats à exclure de l’extrapolation '
                                'en raison d’un nombre de données trop '
                                'faible.')
    analyseur.add_argument('confiance', help='Définition de l’intervalle '
                                             'de confiance demandé (en '
                                             'pourcentage).')
    analyseur.add_argument('nom_fichier', help='Chemin d’accès vers le '
                                               'fichier contenant les '
                                               'sondages à analyser.')
    # Arguments de la ligne de commande.
    args = analyseur.parse_args()

    # Nom du fichier contenant les candidats à exclure de l’extrapolation
    # par manque de données.
    nomExclus = ''
    if args.exclure:
        nomExclus = args.exclure

    try:
        # Intervalle de confiance demandé.
        confiance = float(args.confiance)
    except ValueError as ex:
        print(f'Erreur 1 : impossible de convertir « {args.confiance} » en '
              'un nombre à virgule flottante.', file=sys.stderr)
        sys.exit(1)
    if (confiance <= 0.) or (confiance >= 100.):
        print('Erreur 2: la valeur de l’intervalle de confiance doit être '
              'strictement supérieure à 0 et strictement inférieure à 100.',
              file=sys.stderr)
        sys.exit(2)

    # Chaine de caractère contenant le chemin d’accès vers le fichier de données.
    nomFichier = args.nom_fichier
    if not os.path.exists(nomFichier):
        print(f'Erreur 3 : le fichier « {nomFichier} » n’existe pas.')
        sys.exit(3)
    if not os.path.isfile(nomFichier):
        print(f'Erreur 4 : « {nomFichier} » n’est pas un fichier.')
        sys.exit(4)
    # Chemin d’accès vers le fichier de données.
    chemin_acces = pathlib.Path(nomFichier)

    try:
        app.layout = html.Div(
            children=[html.H1('Sondages'),
                      dcc.Graph(
                          id='scatter',
                          figure=SondageGraph(chemin_acces, confiance).create_fig_from_data_layout(),
                          style={'height': '90vh'}
                      )]
        )

        app.run_server()

        sys.exit(0)

    except NbJoursInco as ex:
        print(f'Erreur 5 : dans le fichier « {ex.nom_fichier} », il n’y a '
              'pas le même nombre de dates de début de sondage que de dates '
              'de fin de sondage.', file=sys.stderr)
        sys.exit(5)
    except NbEchantillonsInco as ex:
        print(f'Erreur 6 : dans le fichier « {ex.nom_fichier} », il n’y a '
              'pas le même nombre de dates de sondage que de nombre '
              'd’échantillons.', file=sys.stderr)
        sys.exit(6)
    except FinAvantDebut as ex:
        print(f'Erreur 7 : dans le fichier « {ex.nom_fichier} », colonne '
              f'{ex.colonne}, le sondage est indiqué comme finissant avant '
              'd’avoir commencé.', file=sys.stderr)
        sys.exit(7)
    except NbIntentionsInco as ex:
        print(f'Erreur 8 : dans le fichier « {ex.nom_fichier} », il y a '
              'incohérence entre la taille de la série des intentions de '
              f'votes envers {ex.nom_candidat} ({ex.taille_serie}) et le '
              f'nombre total de sondages ({ex.nombre_sondages}).',
              file=sys.stderr)
        sys.exit(8)
    except IntentionHorsLimites as ex:
        print(f'Erreur 9 : dans le fichier « {ex.nom_fichier} », pour le '
              f'candidat {ex.nom_candidat}, colonne {ex.colonne}, la valeur '
              'est en-dehors des limites.', file=sys.stderr)
        sys.exit(9)
    except EchantillonInvalide as ex:
        print(f'Erreur 10 : dans le fichier « {ex.nom_fichier} », ligne '
              f'{ex.ligne}, la valeur {ex.colonne} est invalide : elle doit '
              'être un entier.', file=sys.stderr)
        sys.exit(10)
    except EchantillonHorsLimites as ex:
        print(f'Erreur 11 : dans le fichier « {ex.nom_fichier} », ligne '
              f'{ex.ligne}, la valeur {ex.colonne} doit être supérieure ou '
              'égale à 1.', file=sys.stderr)
        sys.exit(11)
    except CouleurInval as ex:
        print(f'Erreur 12 : dans le fichier « {ex.nom_fichier} », la couleur '
              f'associée au candidat {ex.nom_candidat} ne correspond pas à '
              'une couleur disponible.', file=sys.stderr)
        sys.exit(12)
    except JourInvalide as ex:
        print(f'Erreur 13 : dans le fichier « {ex.nom_fichier} », '
              f'ligne {ex.ligne}, la valeur {ex.colonne} est invalide ; '
              'elle doit être un jour codé au format ISO.',
              file=sys.stderr)
        sys.exit(13)
    except EchantillonInval as ex:
        print(f'Erreur 14 : dans le fichier « {ex.nom_fichier} », pour le '
              f'candidat {ex.nom_candidat}, colonne {ex.colonne}, la valeur '
              'est invalide : elle doit être un entier.', file=sys.stderr)
        sys.exit(14)
