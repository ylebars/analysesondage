#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Programme calculant la matrice écarts types – correlations d’une série de
sondages.

:module: calculMatCorr
:author: Yoann LE BARS

Pour le lancer, il suffit d’exécuter le script nommé « calculMatEtCorr.py »
situé dans le répertoire « src ». La syntaxe est la suivante :

::

    calculMatEtCorr.py [-h] [-v] [-f nomFichierSauvegarde] nomFichierSondages

Avec `nomFichierSondages` le chemin d’accès au fichier contenant les sondages
à analyser (il y en a de disponibles dans le répertoire `ressources`).

L’option `-h` (ou `--help`) produit l’affichage d’un message d’aide et met
fin au programme, tandis que l’option `-v` (ou `--version`) produit
l’affichage de la version du programme et met fin à l’exécution. L’option
`-f` (ou `--fichier`) permet de spécifier un fichier où sauvegarder les
valeurs.

Ce programme est sous contrat de distribution CeCILL v2.1. Vous pouvez le
modifier et le redistribuer, à la condition que vous en citiez l’auteur
original et que vous ne changez pas le contrat de distribution.

Le texte complet du contrat est disponible dans le fichier « LICENSE » et à
l’adresse suivante :

https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html
"""


from erreurs import *
from os import path
import csv
import matplotlib.pyplot as plt
import calculEcartType
import calculCorr
from constantes import __version__, negatives, listeReponses


def calcul_matrice(nom_fichier_sondages: os.PathLike,
                   nom_fichier_sauvegarde: os.PathLike) -> None:
    """
    Calcul la matrice écarts types – corrélations d’une série de sondages, la
    stocke dans un fichier CSV et la représente graphiquement.s

    :param os.PathLike nom_fichier_sondages: Chemin d’accès vers le fichier
        contenant les sondages.
    :param os.PathLike nom_fichier_sauvegarde: Chemin d’accès vers le fichier
        où sauvegarder les valeurs.

    :raises FichierInexistant: Si jamais le chemin d’accès vers le fichier
        des sondages est invalide.
    """

    if (not path.exists(nom_fichier_sondages)) or (not path.isfile(nom_fichier_sondages)):
        raise FichierInexistant(nom_fichier_sondages)

    # Liste des candidats à l’élection.
    liste_candidats = []

    with open(nom_fichier_sondages, newline='', encoding='utf-8') as fichier:
        # Lecteur du fichier CSV.
        lecteur = csv.reader(fichier)

        liste_candidats = [line[0] for line in csv.reader(fichier.readlines())][3:]

    # Figure et axes.
    fig, ax = plt.subplots()
    # Liste des écarts types et des corrélations.
    lr = []
    # Liste des valeurs p.
    lp = []
    # Liste des valeurs d’abscisses.
    x = []
    # Liste des valeurs d’ordonnées.
    y = []
    # Indique s’il faut sauvegarder les données.
    sauvegarde = (nom_fichier_sauvegarde != '')
    if sauvegarde:
        fichier = open(nom_fichier_sauvegarde, 'w', newline='', encoding='utf8')
        # Écrivain CSV.
        ecrivain = csv.writer(fichier)
        ecrivain.writerow([''] + liste_candidats)
    # Index du premier candidat courant.
    icandidat1 = 0
    for candidat1 in liste_candidats:
        if sauvegarde:
            # Ligne courante.
            ligne = [candidat1]
        # Index du deuxième candidat courant.
        icandidat2 = 0
        for candidat2 in liste_candidats:
            if candidat1 == candidat2:
                # Écart type concernant le candidat courant.
                r = calculEcartType.traitement(nom_fichier_sondages, candidat1)
                # Valeur p.
                p = 0.
                if sauvegarde:
                    ligne.append('{:.4f}'.format(r))
                ax.text(icandidat1, icandidat1, '{:.2f}'.format(r), ha='center', va='center', color='black')
            else:
                r, p = calculCorr.traitement(nom_fichier_sondages, [candidat1, candidat2])
                if sauvegarde:
                    ligne.append('({:.4f}, {:.4f})'.format(r, p))
                r *= 100.
                p *= 100.
                ax.text(icandidat1, icandidat2, '({:.2f}, {:.2f})'.format(r, p), ha='center', va='center', color='black')
            lr.append(r)
            lp.append(p)
            x.append(candidat1)
            y.append(candidat2)
            icandidat2 += 1
        if sauvegarde:
            ecrivain.writerow(ligne)
        icandidat1 += 1
    if sauvegarde:
        fichier.close()
    # Liste des tailles des points.
    liste_tailles = [100. - p for p in lp]

    # Référence vers la zone de graphe.
    zone = ax.scatter(x, y, s=liste_tailles, c=lr, label=lr, alpha=0.5)
    # Bloc de légende et de dénominations pour les valeurs p.
    bloc, denominations = zone.legend_elements(prop='sizes', alpha=0.6)
    ax.legend(bloc, denominations, loc='upper left', title='Valeur p')
    # Rotation des labels d’abscisse.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         rotation_mode="anchor")
    fig.colorbar(zone)
    ax.set_title('Matrice écarts types – corrélations (en %)')
    plt.show()


# Partie du code à exécuter au début du script.
if __name__ == '__main__':
    import argparse
    import sys

    # Analyseur de la ligne de commande.
    analyseur = argparse.ArgumentParser(
        description='Programme déterminant la matrice écarts type – '
                    'corrélation pour une série de sondages.')
    analyseur.add_argument('-v', '--version', action='version',
                           version='%(prog)s ' + __version__,
                           help='Affiche la version du programme et quitte.')
    analyseur.add_argument('-f', '--fichier', type=str,
                           help='Chemin d’accès vers un fichier où '
                                'sauvegarder les valeurs.')
    analyseur.add_argument('nomFichierSondages',
                           help='Chemin d’accès vers le fichier contenant '
                                'les sondages.')

    # Arguments de la ligne de commande.
    args = analyseur.parse_args()

    # Nom du fichier où sauvegarder les valeurs.
    nom_fichier = ''
    if args.fichier:
        nom_fichier = args.fichier
        if path.exists(nom_fichier):
            if path.isfile(nom_fichier):
                # Réponse de l’utilisateur.
                reponse = input(f'Le fichier « {nom_fichier} » existe déjà, '
                                'voulez-vous l’écraser ([o]ui ou [n]on) ? ')
                while reponse not in listeReponses:
                    print('Réponse non valide.')
                    reponse = input(f'Le fichier « {nom_fichier} » existe '
                                    'déjà, voulez-vous l’écraser ([o]ui ou '
                                    '[n]on) ? ')
                if reponse in negatives:
                    print('Erreur 1 : donnez un autre nom pour le fichier '
                          'de sauvegarde des valeurs.')
                    sys.exit(1)
            else:
                print(f'Erreur 2 : « {nom_fichier} » n’est pas un fichier.',
                      file=sys.stderr)
                sys.exit(2)

    try:
        calcul_matrice(args.nomFichierSondages, nom_fichier)
    except FichierInexistant as ex:
        print(f'Erreur 3 : pas de fichier nommé « {ex.nom_fichier} ».',
              file=sys.stderr)
        sys.exit(3)
    except IntentionInvalide as ex:
        print(f'Erreur 4 : dans le fichier « {ex.nom_fichier} », ligne '
              f'{ex.ligne}, la valeur {ex.colonne} est invalide : elle doit '
              f'être un nombre à virgule flottante.', file=sys.stderr)
        sys.exit(4)
    except IntentionHorsLim as ex:
        print(f'Erreur 5 : dans le fichier « {ex.nom_fichier} », ligne '
              f'{ex.ligne}, la valeur {ex.colonne} doit être supérieure ou '
              f'égale à 0 et inférieure ou égale à 100.', file=sys.stderr)
        sys.exit(5)
    except NomInexistant as ex:
        print(f'Erreur 6 : pas de candidat nommé « {ex.nom} ».',
              file=sys.stderr)
        sys.exit(6)
