Répertoire `ressources`
=======================

Ce répertoire contient des fichiers d’entrées pour l’analyse des sondages de
l’élection présidentielle française de 2022. Il contient les fichiers
suivants :

* `README.md` : le présent fichier, détaillant le contenu du répertoire ;
* `presidentielle-tour1.csv` : la série temporelle des sondages publiés depuis
le 20/01/2021 pour le premier tour de l’élection présidentielle française de
2022 ;
* `presidentielle-tour2-macron-lepen.csv` : la série temporelle des sondages
publiés depuis le 01/02/2019 pour le deuxième tour de l’élection
présidentielle française de 2022, dans le cas d’un deuxième tour entre
Emmanuel MACRON et Marine LE PEN ;
* `presidentielle-tour2-macron-melenchon.csv` : la série temporelle des
sondages publiés depuis le 01/10/2021 pour le deuxième tour de l’élection
présidentielle française de 2022, dans le cas d’un deuxième tour entre
Emmanuel MACRON et Jean-Luc MÉLENCHON ;
* `legislatives-2022-tour1.csv` : la série temporelle des sondages publiés
depuis le 22/04/2022 concernant le premier tour de l’élection législative
française de 2022 ;
* `exclus.csv` : un exemple de fichier permettant d’exclure des candidats de
l’interpolation ;
* `selectionnes.csv` : un exemple de fichier pour sélectionner deux candidats
pour lesquels on veut connaître la corrélation ;
* `matriceCorr-presidentielle2022-tour1.csv` : matrice des corrélations entre
les différents candidats à l’élection présidentielle française de 2022 ;
* `matriceCorr-legislatives-2022-tour1.csv` : matrice des corrélations entre
les différents partis participant à l’élection législative française de
2022 ;
* `matriceEtCorr-presidentielle2022-tour1.csv` : matrice des écarts types et
corrélations entre les différents candidats à l’élection présidentielle
française de 2022 ; 
* `selection-ecartType.txt` : un exemple de fichier pour sélectionner un
candidat pour lequel on veut connaître l’écart type des intentions de votes ;
* `barnier.csv` : la série temporelle des sondages correspondant au cas où
Michel BARNIER aurait été le candidat des Républicains, jusqu’à la primaire ;
* `betrand.csv` : la série temporelle des sondages correspondant au cas où
Xavier BERTRAND aurait été le candidat des Républicains, jusqu’à la primaire ;
* `ciotti.csv` : la série temporelle des sondages correspondant au cas où
Éric CIOTTI aurait été le candidat des Républicains, jusqu’à la primaire ;
* `fillon.csv` : la série temporelle des sondages de l’élection présidentielle
de 2017 pour le candidat François FILLON du 28/11/2016 au 23/04/2017 ;
* `juvin.csv` : la série temporelle des sondages correspondant au cas où
Philippe JUVIN aurait été le candidat des Républicains, jusqu’à la primaire ;
* `montebourg.csv` : série temporelle des intentions de votes concernant
Arnaud MONTEBOURG, du 2 septembre 2021 au 15 décembre 2021.

Les données sont obtenues à partir des sondages validés par la commission des
sondages.
[Une page Wikipédia les recense.](https://fr.wikipedia.org/wiki/Liste_de_sondages_sur_l%27%C3%A9lection_pr%C3%A9sidentielle_fran%C3%A7aise_de_2022 "Liste de sondages sur l'élection présidentielle française de 2022")
