.. AnalyseSondage documentation master file, created by
   sphinx-quickstart on Wed Dec 15 19:47:53 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation de AnalyseSondage
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./analyseSondages.rst
   ./calculCorr.rst
   ./calculMatCorr.rst
   ./calculEcartType.rst
   ./calculCov.rst
   ./calculMatCov.rst
   ./calculMatEtCorr.rst
   ./utilitaires.rst
   ./erreurs.rst
   ./constantes.rst
   ./app.rst
   ./sondage.rst

Indices et tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
