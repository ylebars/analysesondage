Contribuer à ce projet
======================

Toute contribution est la bienvenue !

N’hésitez pas à contacter Yoann LE BARS (ses coordonnées se trouvent dans le
fichier [_AUTHORS_](AUTHORS "AUTHORS")).

**Table des matières**

- [Processus](#processus "Processus")
- [Conventions de codage](#conventions-de-codage "Conventions de codage")

Processus
---------

N’importe qui peut soumettre des modifications. Il est possible de soumettre
aussi bien des _patchs_ que des demandes de fusion de branche. D’une manière
générale, privilégiez de nombreuses petites propositions que quelques
grosses : il est plus facile de résoudre les éventuels conflits dans de
petites propositions que dans des grosses.

Conventions de codage
---------------------

* L’indentation doit être réalisée avec quatre espaces et sans tabulation ;
* il ne devrait pas y avoir d’espace vide à la fin des lignes ;
* conventions de nommage :
    1. les noms de classes commencent avec une majuscule, tandis que les noms
    des objets, variables et fonctions commencent avec des minuscules ;
    2. dans les noms composés de plusieurs mots, dans le cas des variables et
    des noms de fonctions, chaque mot doit être séparé des autres par un
    caractère de soulignement (« _ »), par une majuscule dans le cas des noms
    de classes ;
    3. un objet privé est signalé par un caractère de soulignement en préfixe
    de son nom ;
* conventions pour les commentaires :
    * les commentaires doivent répondre au format
    [Sphinx](https://www.sphinx-doc.org/en/master/ "Sphinx documentation system") ;
    * pour toute classe, méthode, fonction et procédure, la champ `:param:`
    doit être renseigné pour tous les paramètres ;
    * pour toute méthode et fonction, le champ `:returns:` doit être complété ;
    * chaque variable introduite doit être renseignée par un commentaire ;
    * ne pas utiliser le champ `:date:`, dans la mesure où Git conserve une
    trace précise des modifications ;
    * de même, ne pas utiliser le champ `:version:`, qui ne donne pas plus
    d’information que ce que fait Git, sachant que Git est plus précis.
